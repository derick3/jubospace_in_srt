"use strict";

const { exec } = require("child_process");

class JuboSystem extends EventEmitter {
    constructor () {
        super();
        
    }

}
exec("ls -la", (error, stdout, stderr) => {
    if (error) {
        console.log("error: ${error.message}");
        return;
    }
    if (stderr) {
        console.log("stderr: ${stderr}");
        return;
    }
    console.log(stdout);
    console.log('stdout: ${stdout}');
});

exec("hostname -I", (error, stdout, stderr) => {
    if (error) {
        console.log("error: ${error.message}");
        return;
    }
    if (stderr) {
        console.log("stderr: ${stderr}");
        return;
    }
    console.log(stdout);
});