'use strict'

const Nano = require('nano')
const user = 'admin'
const pw = '50868012'
const nanoAddress = '127.0.0.1'
const nanoPort = '5984'
let database = 'http://'+ user + ':' + pw + '@' + nanoAddress + ':' + nanoPort
let nano = Nano(database)

import { iPadDeviceDbInterface,
        mwgMattressDbInterface,
        vayyarDeviceDbInterface,
        philioDeviceDbInterface,
        JuboBoxInfo, 
        nanoDbQueryResp,
        nanoDbDocInterface,
        nanoDbInsertResp,
        nanoDbFindResp 
                                } from './../interfaces/nanoCouchInterface'

interface DeviceDb {
    "dbId": string;
    "keys": Array<DevicePartition>
}

type DevicePartition = {
    "partitionKey" : string;
    "deviceInfo" : string;
}


const dbName = 'jubo-devices-partitioned'
const deviceDbKeys:Array<DevicePartition> = [
                        {partitionKey:'mwg', deviceInfo:""},
                        {partitionKey:'ipad', deviceInfo:""},
                        {partitionKey:'philio',deviceInfo:""},
                        {partitionKey:'vayyar',deviceInfo:""},
                        {partitionKey:'jubobox',deviceInfo:""}]

class JuboDeviceDb {
    dbList: Array <string>;
    deviceDbHandler: any;
    devicesDb: DeviceDb;
    devicesInfo: any;
    timerHandler: any;
    dbReady: boolean;
    iPadDeviceList: Array<iPadDeviceDbInterface>
    mwgMattressList: Array<mwgMattressDbInterface>
    vayyarDeviceList: Array<vayyarDeviceDbInterface>
    philioDeviceList: Array<philioDeviceDbInterface>

    constructor () {
        this.dbReady = false
        this.iPadDeviceList=[]
        this.deviceDbHandler = nano.db.use(dbName)
        this.devicesDb = {
            dbId: "",
            keys: deviceDbKeys
        }
        this.getDbList().catch((error)=>{
            console.log(`err: JuboDeviceDb Constructor getDbList() ${error}`)
        }).then((result : Array<string>)=>{
            this.dbList = result
            return result         
        }).then((dbList:Array<string>)=>{
            if(dbList.includes(dbName) && this.deviceDbHandler != undefined){
                this.getIpadDeviceList().then((result)=>{
                    this.iPadDeviceList = result
                })

                this.getVayyarDeviceList().then((result)=>{
                    this.vayyarDeviceList = result
                })

                this.getMwgMattressDeviceList().then((result)=>{
                    this.mwgMattressList = result
                })

                this.getPhilioDeviceList().then((result)=>{
                    this.philioDeviceList = result
                })

            } else {
                throw new Error('JuboDeviceDb Constructor Initialize Error')
            }
        })

        this.timerHandler = setInterval(this.dbTimerRoutine.bind(this),60000)
    }

    private dbTimerRoutine = () => {
        try {

        } catch (error) {
            console.log(`fn: dbTimerRoutine ${error}`)
        }
    }

    private getDbList = () : Promise <string[]>=> {
        return new Promise((resolve,reject) => {
            try{
                resolve(nano.db.list())

            } catch (error) {
                reject(error)
            }
        })
    }

    private nanoDeleteDoc = (docId:string,docRev:string):Promise<any> =>{
        return new Promise((resolve,reject)=>{
            try{
                this.deviceDbHandler.destroy(docId,docRev).then((result)=>{
                    console.log(result)
                    resolve(result)
                })
            } catch(error) {
                console.log(`fn:nanoDeleteDoc ${error}`)
                reject(error)
            }
        })
    }

    private nanoDeleteDocBulk = (bulkDocs:Array<nanoDbDocInterface>):Promise<any> =>{
        return new Promise((resolve,reject)=>{
            try{
                for(const doc of bulkDocs){
                    this.nanoDeleteDoc(doc._id,doc._rev).then((resp:nanoDbInsertResp)=>{
                        if(resp.ok === false){
                            reject(new Error("Doc Delete Fail"))
                        }
                    })
                }
                resolve('Delete Success')
            } catch(error) {
                reject(error)
            }
        })
    }
    private getDeviceListByKey = (partitionKey:string) => {
        return new Promise((resolve,reject) => {
            try{
                resolve(this.deviceDbHandler.partitionedList(partitionKey,{ include_docs: true }))
                
            } catch(error) {
                reject(error)
            }
        })
    }

    public getJuboBoxInfo = (): Promise<JuboBoxInfo> => {
        return new Promise((resolve,reject)=>{
            try{
                this.getDeviceListByKey('jubobox').then((result:any)=>{
                    console.log(result)
                    resolve(result.rows[0].doc)
                })
            } catch(error){
                console.log(`fn:getJuboBoxInfo ${error}`)
                reject(error)
            }
        })
    }

    public getVayyarDeviceList = (): Promise<vayyarDeviceDbInterface[]> => {
        return new Promise((resolve,reject)=>{
            try{
                this.getDeviceListByKey('vayyar').then((result:any)=>{
                    let devices:Array<nanoDbQueryResp> = result.rows
                    let vayyarDevices:Array<vayyarDeviceDbInterface> = []
                    for(const device of devices){
                        vayyarDevices.push(device.doc.vayyarDevice)
                    }
                    resolve(vayyarDevices)
                })
                
            } catch(error){
                reject(error)
            }
        })
    }

    public getMwgMattressDeviceList = (): Promise<mwgMattressDbInterface[]> => {
        return new Promise((resolve,reject)=>{
            try{
                this.getDeviceListByKey('mwg').then((result:any)=>{
                    let devices:Array<nanoDbQueryResp> = result.rows
                    let mwgMattresses:Array<mwgMattressDbInterface> = []
                    for(const device of devices){
                        mwgMattresses.push(device.doc.mwgMattress)
                    }
                    resolve(mwgMattresses)
                })
            } catch(error){
                reject(error)
            }
        })
    }

    public getIpadDeviceList = () : Promise<iPadDeviceDbInterface[]> => {
        return new Promise((resolve,reject) => {
            try{
                this.getDeviceListByKey('ipad').then((result:any)=>{
                    let devices:Array<nanoDbQueryResp> = result.rows
                    let iPads:Array<iPadDeviceDbInterface> = []
                    for(const device of devices){
                        iPads.push(device.doc.iPadDevice) 
                    }
                    resolve(iPads)
                })
            } catch (error){
                reject(error)
            }
        })
    }

    public getPhilioDeviceList = () : Promise<philioDeviceDbInterface[]> => {
        return new Promise((resolve,reject)=>{
            try{
                this.getDeviceListByKey('philio').then((result:any)=>{
                    let devices:Array<nanoDbQueryResp> = result.rows
                    let philios:Array<philioDeviceDbInterface> = []
                    for(const device of devices){
                        philios.push(device.doc.philioDevice)
                    }
                    resolve(philios)
                })
            } catch(error){
                reject(error)
            }
        })
    }    

    public findIpad = (iPad:iPadDeviceDbInterface):Promise<nanoDbFindResp> => {
        return new Promise((resolve,reject)=>{
            try{
                const mangoQuery = {
                    selector: {
                        iPadDevice: {
                            deviceId: { "$eq": iPad.deviceId }
                        }
                    },
                    fields: [ "iPadDevice","_id","_rev" ]
                };
                this.deviceDbHandler.find(mangoQuery).then((result) => {
                    resolve(result)
                })
            } catch(error){
                console.log(`fn:findIpad ${error}`)
                reject(error)
            }
        })
    }

    public findMwgBed = (mwg:mwgMattressDbInterface):Promise<nanoDbFindResp> => {
        return new Promise((resolve,reject)=>{
            try{
                const mangoQuery = {
                    selector: {
                        mwgMattress: {
                            deviceId: {"$eq": mwg.deviceId }
                        }
                    },
                    fields: [ "mwgMattress","_id","_rev"]
                };
                this.deviceDbHandler.find(mangoQuery).then((result)=>{
                    resolve(result)
                })
            } catch(error){
                console.log(`fn:findMwgBed ${error}`)
                reject(error)
            }
        })
    }

    public findVayyarSensor = (vayyar:vayyarDeviceDbInterface):Promise<nanoDbFindResp> =>{
        return new Promise((resolve,reject)=>{
            try{
                const mangoQuery = {
                    selector: {
                        vayyarDevice: {
                            deviceId: {"$eq": vayyar.deviceId}
                        }
                    },
                    fields: [ "vayyarDevice", "_id", "_rev" ]
                };
                this.deviceDbHandler.find(mangoQuery).then((result)=> {
                    resolve(result)
                })
            } catch(error){
                console.log(`fn:findVayyarSensor ${error}`)
                reject(error)
            }
        })
    }

    public dbInsertUpdateJuboBox = (jubobox:JuboBoxInfo):Promise<nanoDbInsertResp> => {
        return new Promise((resolve,reject)=>{
            try{
                this.deviceDbHandler.insert(jubobox).then((resp:nanoDbInsertResp)=>{
                    resolve(resp)
                })
            } catch(error){
                console.log(`fn:dbInsertUpdateJubobox ${error}`)
                reject(error)
            }
        })
    }

    public dbInsertUpdateIpad = (iPad:iPadDeviceDbInterface):Promise<nanoDbInsertResp> => {
        return new Promise((resolve,reject)=>{
            try{
                this.findIpad(iPad).then((doc:nanoDbFindResp) => {
                    //console.log(doc)
                    if(doc.docs.length===0){
                        //insert new doc
                        //console.log(iPad)
                        nano.uuids(2).then((result)=>{
                            //console.log(result)
                            let uids = result.uuids
                            this.deviceDbHandler.insert({
                                _id: 'ipad:'+uids[0],
                                _rev: '0-'+uids[1],
                                iPadDevice: iPad
                            }).then((resp:nanoDbInsertResp)=>{
                                resolve(resp)
                            }).catch((error)=>{
                                if(error.statusCode === 409){
                                    console.log(error.description)
                                    console.log(error.message)
                                } else {
                                    console.log(error.description)
                                }
                            })   
                        })
                    } else if(doc.docs.length===1) {
                        //update doc
                        let repeated = doc.docs[0]
                        this.deviceDbHandler.insert({
                            _id: repeated._id,
                            _rev: repeated._rev,
                            iPadDevice: repeated.iPadDevice
                        }).then((resp:nanoDbInsertResp)=>{
                            resolve(resp)
                        })

                    } else {
                        //Conflict more then 2 device ID, need to kill all then insert new
                        //destroy all then insert new
                        let docs:Array<nanoDbDocInterface> = doc.docs
                        this.nanoDeleteDocBulk(docs).then((resp)=>{
                            if(resp==='Delete Success'){
                                let failResp: nanoDbInsertResp ={
                                    "ok":false,
                                    "failCode": "409",
                                    "failMessage": "Duplicates Deleted"
                                }
                                resolve(failResp)                        
                            } else {
                                reject(new Error('Fail to Delete Duplicates'))
                            }
                        })
                    }       //end else Conflict
                });
            } catch(error){
                console.log(`fn:addNewIpad ${error}`)
                reject(error)
            }
        })
    }

    public dbInsertUpdateVayyarDevice = (vayyar:vayyarDeviceDbInterface):Promise<nanoDbInsertResp> => {
        return new Promise((resolve,reject)=>{
            try{
                this.findVayyarSensor(vayyar).then((doc:nanoDbFindResp)=>{
                    console.log(doc)
                    if(doc.docs.length===0){
                        nano.uuids(2).then((result)=>{
                            console.log(result)
                            let uids = result.uuids
                            this.deviceDbHandler.insert({
                                _id: 'vayyar:'+uids[0],
                                _rev: '0-'+uids[1],
                                vayyarDevice: vayyar
                            }).then((resp:nanoDbInsertResp)=>{
                                resolve(resp)
                            }).catch((error)=>{
                                if(error.statusCode === 409){
                                    console.log(error.description)
                                    console.log(error.message)
                                } else {
                                    console.log(error.description)
                                }
                            })   
                        })
                    } else if(doc.docs.length===1){
                        let repeated = doc.docs[0]
                        this.deviceDbHandler.insert({
                            _id: repeated._id,
                            _rev: repeated._rev,
                            vayyarDevice: repeated.vayyarDevice
                        }).then((resp:nanoDbInsertResp)=>{
                            resolve(resp)
                        })
                    } else {
                        //Conflict more then 2 device ID, need to kill all then insert new
                        //destroy all then insert new
                        let docs: Array<nanoDbDocInterface> = doc.docs
                        this.nanoDeleteDocBulk(docs).then((resp)=>{
                            if(resp==='Delete Success'){
                                let failResp: nanoDbInsertResp = {
                                    "ok":false,
                                    "failCode": "409",
                                    "failMessage": "Duplicates Deleted"
                                }
                                resolve(failResp)
                            } else {
                                reject(new Error('Fail to Delete Duplicates'))
                            }
                        })
                    }   //end else Conflict
                })

            } catch(error){
                console.log(`fn:addNewVayyarDevice ${error}`)
                reject(error)
            }
        })
    }

    public dbInsertUpdateMwgMattress = (mwg:mwgMattressDbInterface):Promise<nanoDbInsertResp> => {
        return new Promise((resolve,reject)=>{
            try{
                this.findMwgBed(mwg).then((doc:nanoDbFindResp)=> {
                    //console.log(doc)
                    if(doc.docs.length===0){
                        //insert new doc
                        console.log(mwg)
                        nano.uuids(2).then((result)=>{
                            //console.log(result)
                            let uids = result.uuids
                            this.deviceDbHandler.insert({
                                _id: 'mwg:'+uids[0],
                                _rev: '0-'+uids[1],
                                mwgMattress: mwg
                            }).then((resp:nanoDbInsertResp)=>{
                                resolve(resp)
                            }).catch((error)=>{
                                if(error.statusCode === 409){
                                    console.log(error.description)
                                    console.log(error.message)
                                } else {
                                    console.log(error.description)
                                }
                            })
                        })
                    } else if(doc.docs.length===1){
                        //update doc
                        let repeated = doc.docs[0]
                        this.deviceDbHandler.insert({
                            _id: repeated._id,
                            _rev: repeated._rev,
                            mwgMattress: repeated.mwgMattress
                        }).then((resp:nanoDbInsertResp)=>{
                            resolve(resp)
                        })
                    } else {
                        //Conflict more then 2 device ID, need to kill all then insert new
                        //destroy all then insert new
                        let docs:Array<nanoDbDocInterface> = doc.docs
                        this.nanoDeleteDocBulk(docs).then((resp)=>{
                            if(resp==='Delete Success'){
                                let failResp: nanoDbInsertResp = {
                                    ok: false,
                                    failCode: "409",
                                    failMessage: "Duplicates Deleted"
                                }
                                resolve(failResp)
                            } else {
                                reject(new Error('Fail to Delete Duplicates'))
                            }
                        })                        
                    }   //end else Conflict
                })

            } catch(error){
                console.log(`fn:addNewMwgMattress ${error}`)
                reject(error)
            }
        })
    }


    public dbInsertUpdatePhilio = (philio:philioDeviceDbInterface):Promise<nanoDbInsertResp> => {
        return new Promise((resolve,reject)=>{
            try{

            } catch(error){
                console.log(`fn:dbInsertUpdatePhilio ${error}`)
                reject(error)
            }
        })
    }
}


/*
const testDb = new JuboDeviceDb()
const test = async () => {
    testDb.dbInsertUpdateIpad({
        "deviceId": '1225F11E-241F-13A9-8E6C-214BC8BF5ED4',
        "vendorId": '',
        "deviceName": 'iPad - Jubo Space App',
        "patientId": '',
        "location": {
            "facility":'仁康護理之家',
            "roomNum":'12345',
            "position":'ipad Stand'},
        "createdTime": 'string',
        "updatedTime": 'string',
        "lastBeatTime": 'string'        
    }).then((result:nanoDbInsertResp)=>{
        console.log(result)
    })

    testDb.getIpadDeviceList().then((result)=>{
      //  console.log(result)
    })
    testDb.getMwgMattressDeviceList().then((result)=>{
      //  console.log(result)
    })
    testDb.getVayyarDeviceList().then((result)=>{
      //  console.log(result)
    })
    testDb.getPhilioDeviceList().then((result)=>{
       // console.log(result)
    })


    
}

test()
*/


module.exports = JuboDeviceDb







