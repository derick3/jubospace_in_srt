const fs = require('fs')

const Nano = require('nano')
const user = 'admin'
const pw = '50868012'
const nanoAddress = '127.0.0.1'
const nanoPort = '5984'
let database = 'http://'+ user + ':' + pw + '@' + nanoAddress + ':' + nanoPort
let nano = Nano(database)

const utils = require('./../utils/utilities')
const philioApiHandler = require('./../stateMachine/philioZwave/philioApi/philioApiHandler')
const philioSysControl = require('./../stateMachine/philioZwave/systemControl/philioSystemControl')
const philioApi = require('./../stateMachine/philioZwave/philioApi/philioDeviceApi')

import  { DeviceLocation, 
        nanoDbInsertResp, 
        nanoDbFindResp,
        PhilioDevice, 
        PhilioSensor, 
        PhilioActuator,
        EventDbFormatOutput,
        SensorSeriesInterface,
        EventSeriesInterface,
        SensorDataType,
        EventDataType,
        ActuatorFunction,
        ActuatorChannelDetail,
        SensorFunction,
        SensorChannelDetail } from '../interfaces/nanoCouchInterface'

import { PhilioRespDevice, 
            PhilioRespDeviceChannel,
            PhilioEventMessage } from '../stateMachine/philioZwave/interfaces/philioZwaveInterface'

const dbName = 'philio-system-partitioned'
const deviceDbKeys:Array<any> = [
    {pKey:'',pType:'device'},
    {pKey:'',pTYpe:'scene'}
]

class PhilioSystemDb {
    timerHandler:any
    philioDbHandler:any

    constructor() {
        this.philioDbHandler = nano.db.use(dbName)
        this.timerHandler = setInterval(this.dbTimerRoutine.bind(this),60000)
    }

    private dbTimerRoutine = () => {
        try {

        } catch (error) {
            console.log(`fn:dbTimerRoutine ${error}`)
        }
    }

    public dbFindDeviceByVendorId = (philio:PhilioDevice|PhilioSensor|PhilioActuator):Promise<any>=>{
        return new Promise((resolve,reject)=>{
            try{
                const mangoQuery = {
                    selector: {
                        philioDevice: {
                            vendorId: { "$eq": philio.vendorId }
                        }
                    },
                    fields: [ "philioDevice","_id","_rev"]
                };
                this.philioDbHandler.find(mangoQuery).then((result)=>{
                    resolve(result)
                })
            } catch(error){
                console.log(`fn: dbFindPhilioDevice ${error}`)
                reject(error)
            }
        })
    }

    public dbAddNewPhilioDevice = (philio:PhilioDevice):Promise<nanoDbInsertResp> => {
        return new Promise((resolve,reject)=>{
            try{
                nano.uuids(3).then((result)=>{
                    let partitionName = ''
                    let uids = result.uuids
                    philio.deviceId = uids[2]

                    switch(philio.deviceType){
                        case 'actuator': partitionName = philio.vendorId+'#actuator'; break;
                        case 'sensor': partitionName = philio.vendorId+'#sensor'; break;
                        case 'dimmer': partitionName = philio.vendorId+'#dimmer'; break;
                        default: partitionName = ''; break;
                    }
                    console.log(partitionName)

                    this.philioDbHandler.insert({
                        _id: partitionName+':'+uids[0],
                        //_rev: '0-'+uids[1],
                        philioDevice: philio
                    }).then((resp:nanoDbInsertResp)=>{
                        resolve(resp)
                    }).catch((error)=>{
                        console.log(error)
                    })
                })
            } catch(error) {
                console.log(`fn: dbAddNewPhilioDevice ${error}`)
                reject(error)
            }
        })
    }

    public dbUpdatePhilioDevice = (dbResult:nanoDbFindResp,philio:PhilioDevice):Promise<nanoDbInsertResp> => {
        return new Promise((resolve,reject)=>{
            try{
                //console.log(dbResult)
                let dbDoc = dbResult.docs[0]
                let philioDeviceUpdate = dbDoc.philioDevice

                philioDeviceUpdate.createdTime = dbDoc.philioDevice.createdTime
                philioDeviceUpdate.lastBeatTime = dbDoc.philioDevice.lastBeatTime
                philioDeviceUpdate.deviceId = dbDoc.philioDevice.deviceId
                dbDoc.philioDevice = philioDeviceUpdate

                this.philioDbHandler.insert(dbDoc).then((resp:nanoDbInsertResp)=>{
                    resolve(resp)
                })
            } catch(error){
                console.log(`fn: dbUpdatePhilioDevice ${error} `)
            }
        })
    }

    public dbDeletePhilioDevice = (philio:PhilioDevice):Promise<nanoDbInsertResp> => {
        return new Promise((resolve,reject)=>{
            return new Promise((resolve,reject)=>{
                try{

                } catch(error){

                }
            })
        })
    }

    public dbFindEventSeries = (eventLog:EventSeriesInterface):Promise<any> =>{
        return new Promise((resolve,reject)=>{
            try{

                const mangoQuery = {
                    selector: {
                        eventLog: {
                            vendorId: { "$eq": eventLog.vendorId },
                            channelId: { "$eq": eventLog.channelId }
                        }
                    }, 
                    fields: [ "eventLog", "_id", "_rev" ]
                }
                this.philioDbHandler.find(mangoQuery).then((result)=>{
                    resolve(result)
                })
            } catch(error){
                console.log(`fn: dbFindEventSeriesByVendorId ${error}`)
                reject(error)
            }
        })
    }

    public dbFindSensorSeries = (sensorLog:SensorSeriesInterface):Promise<any> =>{
        return new Promise((resolve,reject)=>{
            try{
                
                const mangoQuery = {
                    selector: {
                        sensorLog: {
                            vendorId: { "$eq": sensorLog.vendorId },
                            channelId: { "$eq": sensorLog.channelId }
                        }
                    }, 
                    fields: [ "sensorLog", "_id", "_rev" ]
                }
                this.philioDbHandler.find(mangoQuery).then((result)=>{
                    resolve(result)
                })
            } catch(error){
                console.log(`fn: dbFindEventSeriesByVendorId ${error}`)
                reject(error)
            }
        })
    }    

    public dbUpdateDeviceEvent = (eventLog:Array<EventSeriesInterface>):Promise<any> => {
        return new Promise((resolve,reject)=>{
            try {
                console.log(eventLog)
            } catch(error){

            }
        })
    }

    public dbAddNewSensorDataSeries = (sensorLog: SensorSeriesInterface):Promise<nanoDbInsertResp> => {
        return new Promise((resolve,reject)=>{
            try{
                //console.log(sensorLog.sensorType)
                let partitionName:string = ''
                switch(sensorLog.sensorType){
                    case 'Humidity Sensor': partitionName=sensorLog.vendorId+'#humidity'; break;
                    case 'Temperature Sensor': partitionName=sensorLog.vendorId+'#temperature'; break;
                    default: partitionName=''; break;
                } 
                //console.log(partitionName)
                sensorLog.sensorSeries = sensorLog.sensorSeries.sort((y,x)=> y.timeStamp - x.timeStamp)

                nano.uuids(2).then((result)=>{
                    let uids = result.uuids
                    this.philioDbHandler.insert({
                        _id: partitionName+':'+uids[0],
                        //_rev: '0-'+uids[1],
                        sensorLog: sensorLog
                    }).then((resp:nanoDbInsertResp)=>{
                        resolve(resp)
                    })
                })
            } catch(error){
                console.log(`fn: dbAddNewSensorSeries ${error}`)
                reject(error)
            }
        })
    }

    public dbUpdateSensorSeries = (dbResult:nanoDbFindResp, newSensorSeries: Array<SensorDataType>):Promise<nanoDbInsertResp> => {
        return new Promise((resolve,reject)=>{
            try{
                let dbDoc = dbResult.docs[0]
                let dbSensorLog = dbDoc.sensorLog
                let dbSensorSeries = dbSensorLog.sensorSeries

                dbSensorLog.updatedTimeStamp = new Date(Date.now()).toISOString()
                dbSensorLog.lastSensorValue = newSensorSeries[newSensorSeries.length -1].sensorVal
                dbSensorLog.sensorSeries = dbSensorSeries.concat(newSensorSeries).sort((y,x)=>y.timeStamp-x.timeStamp)

                dbDoc.sensorLog = dbSensorLog
                this.philioDbHandler.insert(dbDoc).then((resp:nanoDbInsertResp)=>{
                    resolve(resp)
                })
            } catch (error){
                console.log(`fn: dbUpdateSensorSeries ${error}`)
                reject(error)
            }
        })
    }

    public dbAddNewAbnormalDataSeries = (abnormalLog: EventSeriesInterface): Promise<nanoDbInsertResp> => {
        return new Promise((resolve,reject)=>{
            try{
                let partitionName:string = ''

                switch(abnormalLog.eventType){
                    case 'tamper_detect': partitionName=abnormalLog.vendorId+'#tamper'; break;
                    case 'low_battery': partitionName=abnormalLog.vendorId+'#lowBattery'; break;
                    case 'battery_change': partitionName=abnormalLog.vendorId+'#batteryChange'; break;
                    default: partitionName=''; break;
                }
                abnormalLog.eventSeries = abnormalLog.eventSeries.sort((y,x) => y.timeStamp - x.timeStamp)

                nano.uuids(2).then((result)=>{
                    let uids = result.uuids
                    this.philioDbHandler.insert({
                        _id: partitionName+':'+uids[0],
                        //_rev: partitionName+'0-'+uids[1],
                        eventLog: abnormalLog
                    }).then((resp)=>{
                        resolve(resp)
                    })
                })

            } catch(error){
                console.log(`fn: dbAddNewAbnormalDataSeries ${error}`)
                reject(error)
            }
        })
    }

    public dbUpdateAbnormalDataSeries = (dbResult:nanoDbFindResp ,newAbnormalSeries: Array<EventDataType>): Promise<nanoDbInsertResp> => {
        return new Promise ((resolve, reject)=>{
            try{
                let dbDoc = dbResult.docs[0]
                let dbEventLog = dbDoc.eventLog
                let dbAbnormals = dbEventLog.eventSeries

                dbEventLog.updatedTimeStamp = new Date(Date.now()).toISOString()
                dbEventLog.lastEventVal = newAbnormalSeries[newAbnormalSeries.length-1].eventVal.toString()
                dbEventLog.eventSeries = dbAbnormals.concat(newAbnormalSeries).sort((y,x)=>y.timeStamp-x.timeStamp)

                dbDoc.eventLog = dbEventLog
                this.philioDbHandler.insert(dbDoc).then((resp:nanoDbInsertResp)=>{
                    resolve(resp)
                })
                
            } catch(error){
                console.log(`fn: dbUpdateAbnormalDataSeries ${error}`)
                reject(error)
            }
        })
    }

    public dbAddNewMotionDataSeries = (motionLog: EventSeriesInterface) : Promise<nanoDbInsertResp> => {
        return new Promise((resolve,reject)=>{
            try{
                let partitionName:string = ''
                
                switch(motionLog.eventType){
                    case 'door': partitionName=motionLog.vendorId+'#door'; break;
                    case 'pir': partitionName=motionLog.vendorId+'#pir'; break;
                    default: partitionName=''; break;
                }

                motionLog.eventSeries = motionLog.eventSeries.sort((y,x)=> y.timeStamp - x.timeStamp)
                
                //console.log(partitionName)
                nano.uuids(2).then((result)=>{
                    let uids = result.uuids
                    this.philioDbHandler.insert({
                        _id: partitionName+':'+uids[0],
                        //_rev: '0-'+uids[1],
                        eventLog: motionLog
                    }).then((resp:nanoDbInsertResp)=>{
                        resolve(resp)
                    })
                })                
            } catch(error){
                console.log(`fn: dbAddNewMotionLog ${error}`)
                reject(error)
            }
        })
    }

    public dbUpdateMotionDataSeries = (dbResult:nanoDbFindResp, newMotionSeries: Array<EventDataType>): Promise<nanoDbInsertResp> => {
        return new Promise((resolve,reject)=>{
            try{
                let dbDoc = dbResult.docs[0]
                let dbEventLog = dbDoc.eventLog
                let dbMotions = dbEventLog.eventSeries

                dbEventLog.updatedTimeStamp = new Date(Date.now()).toISOString()
                dbEventLog.lastEventVal = newMotionSeries[newMotionSeries.length-1].eventVal.toString()
                dbEventLog.eventSeries = dbMotions.concat(newMotionSeries).sort((y,x)=> y.timeStamp - x.timeStamp)

                dbDoc.eventLog = dbEventLog
                this.philioDbHandler.insert(dbDoc).then((resp:nanoDbInsertResp)=>{
                    resolve(resp)
                })
            } catch(error){
                console.log(`fn: dbUpdateMotionDataSeries ${error}`)
                reject(error)
            }
        })
    }

    public dbUpdateActuatorLog = ():Promise<any> =>{
        return new Promise((resolve,reject)=>{
            try{

            } catch(error){

            }
        })
    }

    public dbDeleteEventSeries = ():Promise<any> => {
        return new Promise((resolve,reject)=>{
            try{

            } catch(error){
                
            }
        })
    }
}

// test Code
let ifConfig = {
    deviceVendor: 'Philio Technology Corporation',
    deviceIp: '192.168.69.53',
    //deviceIp: '192.168.2.37',
    deviceMac: '18:cc:23:00:8e:da',
    //deviceMac: '18:cc:23:00:8e:ca',
    deviceHostname: null,
    isDeviceAlive: true,
    connectOption: {
      uri: '',
      port: 80,
      method: 'GET',
      auth: { user: 'admin', pass: '888888', sendImmediately: true }
    }
  }

module.exports = PhilioSystemDb;

