"use strict";

const netlist = require('network-list');
const os = require('os');
const ifaces = os.networkInterfaces();

const find = (deviceName) => {
    return new Promise((resolve,reject) => {
        if (deviceName == "Self") {

            let selfIp = [];
            Object.keys(ifaces).forEach(function (ifname) {
                let alias = 0;
                // console.log(ifname)
                ifaces[ifname].forEach(function (iface) {
                    if ('IPv4' !== iface.family || iface.internal !== false) {
                        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
                        return iface.address;
                    }

                    if (alias >= 1) {
                        // this single interface has multiple ipv4 addresses
                        //console.log(ifname + ':' + alias, iface.address);
                        selfIp.push(iface.address);
                        // console.log(selfIp)
                        
                    } else {
                        // this interface has only one ipv4 adress
                        //console.log(ifname, iface.address);
                        selfIp.push(iface.address);
                        // console.log(selfIp)
                    }
                    ++alias;
                });
            });

            netlist.scanEach({}, function (err, device) {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                if (device.alive != false) {
 
                    if (selfIp.find(element => element == device.ip)) {
                        // console.log(device)     
                        console.log("Netlist found: self")           
                        let resolver = createJuboDeviceObject(device, deviceName);
                        
                        // console.log("resolving:" + resolver);
                        resolve(resolver);
                    }
                }
            });
        } else {
            let resolver;
           // while(resolver==null){
                netlist.scanEach({}, (err, device) => {
                    if (err) {
                        console.log(err);
                        reject(err);
                    }
                    // console.log(device)
                    if (device.hostname != null || device.vendor != null) {
                        let searchString = device.hostname + device.vendor;
                        // console.log(device);
                        if (searchString.includes(deviceName) == true) {
                            
                            let resolver = createJuboDeviceObject(device, deviceName);
                            console.log(`${searchString} Found`)
                            //console.log(device)
                            // console.log(resolver);
                            resolve(resolver);
    
                        } else {
                            
                        }
                    } else {
                        
                    }
                });
           // }

        }        
    }); //return Promise
}   // find()


const createJuboDeviceObject = (
    deviceInfo,
    deviceName) => {
    // console.log(deviceInfo)
    if (deviceName == null) {
        let newDevice = {
            deviceVendor: "",
            deviceIp: "",
            deviceMac: "",
            deviceHostname: "",
            isDeviceAlive: false
        };

        ({
            ip: newDevice.deviceIp,
            mac: newDevice.deviceMac,
            vendor: newDevice.deviceVendor,
            hostname: newDevice.deviceHostname,
            alive: newDevice.isDeviceAlive
        } = deviceInfo);

        return newDevice;
    } else if (deviceName == 'Philio') {

        let options = {
            uri: "",
            port: 80,
            method: 'GET',
            auth: {
                'user': 'admin',
                'pass': '888888',
                'sendImmediately': true
            }
        };

        let newDevice = {
            deviceVendor: deviceInfo.vendor,
            deviceIp: deviceInfo.ip,
            deviceMac: deviceInfo.mac,
            deviceHostname: deviceInfo.hostname,
            isDeviceAlive: true,
            connectOption: options
        };

        //console.log(newDevice)
        return newDevice;

    } else if (deviceName == 'Self') {
        let options = {
            uri: "",
            port: 80,
            method: 'GET',
            auth: {
                'user': "secret",
                'pass': "secret",
                'sendImmediately': true
            }
        };

        let newDevice = {
            deviceVendor: deviceInfo.vendor,
            deviceIp: deviceInfo.ip,
            deviceMac: deviceInfo.mac,
            deviceHostname: deviceInfo.hostname,
            isDeviceAlive: true,
            connectOption: options
        };
        // console.log(newDevice)
        return newDevice;
    }
}   // end createJuboDeviceObject


async function findSelf(){
    return await find("Self");
}

async function findPhilio(){
    return await find("Philio");

}

exports.find = find;
exports.findSelf = findSelf;
exports.findPhilio = findPhilio;
