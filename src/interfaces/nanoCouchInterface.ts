export interface nanoDbQueryResp {
    "id": string;
    "key": string;
    "value": {
        "rev": string;
    }
    "doc": nanoDbDocInterface;
}

export interface nanoDbDocInterface {
    "_id": string;
    "_rev": string;
    "sensorLog"?: SensorSeriesInterface;
    "eventLog"?: EventSeriesInterface;
    "iPadDevice"?: iPadDeviceDbInterface;
    "mwgMattress"?: mwgMattressDbInterface;
    "vayyarDevice"?: vayyarDeviceDbInterface;
    "philioDevice"?: philioDeviceDbInterface;

}

export interface nanoDbInsertResp {
    "ok"?: boolean;
    "id"?: string;
    "rev"?: string;
    "failCode"?: string;
    "failMessage"?: string;
}

export interface nanoDbFindResp {
    "docs": Array<nanoDbDocInterface>;
    "bookmark"?: string;
    "warning"?: string;
}




export interface iPadDeviceDbInterface extends genericDeviceDbInterface{

}

export interface philioDeviceDbInterface extends genericDeviceDbInterface{

}

export interface mwgMattressDbInterface extends genericDeviceDbInterface{

}

export interface vayyarDeviceDbInterface extends genericDeviceDbInterface{

}

export interface genericDeviceDbInterface {
    "deviceId": string;
    "vendorId": string;
    "deviceName": string;
    "patientId": string;
    "location": DeviceLocation;
    "createdTime": string;
    "updatedTime": string;
    "lastBeatTime": string;
}

export type DeviceLocation = {
    "facility": string;
    "roomNum": string;
    "position": string;
    "bedNum"?: string;
    "numOfBeds"?: string;
    "maxPatients"?: string;
}

export interface PatientInfo {
    "patientId": string;
    "first_name"?: string;
    "last_name"?: string;
    "display_name"?: string;
    "phone"?: string;
    "birthday"?: string;
    "sex"?: StringConstructor;
    "email"?: string;
    "id_number"?: string;
    "location"?:PatientLocation;
  }

  type PatientLocation =  {
    "facility": string;
    "roomNum": string;
    "bedNum": string;
    "address"?: string;
  }

export interface JuboBoxInfo {
    "deviceId": string;
    "vendorId": string;
    "deviceName": string;
    "connectedDeviceList": Array<simplifyDeviceInfo>;
    "patientList": Array<simplifyPatientInfo>;
    "location": DeviceLocation;
    "createdTime": string;
    "updatedTime": string;
    "lastBeatTime":string;
}

export type simplifyDeviceInfo = {
    "deviceId": string;
    "vendorId": string;
    "devicePartitionKey": string;
    "registeredPatientId": string;
}

export type simplifyPatientInfo = {
    "patientId": string;
    "facilityPartitionKey": string;
    "registeredIpadId": string;

}

export interface PhilioActuator extends PhilioDevice {
    
    
}
export interface PhilioSensor extends PhilioDevice {

}

export interface PhilioDevice {
    "deviceId": string;
    "vendorId": string;
    "deviceName": string;
    "description": string;
    "deviceType": Array<string>|string;      //sensor, actuator
    "location":DeviceLocation;
    "createdTime": string;
    "updatedTime": string;
    "lastBeatTime": string;
    "currentStatus": string;
    "powerSource": string;
    "currentPowerLevel": string;   
    "functions"?: Array<ActuatorFunction|SensorFunction|MeterFunction>;

}

export type ActuatorFunction = {
    "channels" : ActuatorChannelDetail;
    "type": string;  //state, range
    "on"?: string;      //state
    "off"?: string;     //state
    "min"?: string;     //range
    "max"?: string;     //range
}

export type ActuatorChannelDetail = {
    "chid": string;
    "currentState": string;
}

export type SensorFunction = {
    "channels" : SensorChannelDetail;
}

export type SensorChannelDetail = {
    "chid" : string;
    "name": string;
    "sensorUnit": string;
    "valueScale": number;
    "lastValue": string;
}

type MeterFunction = {

}

export interface EventDbFormatOutput {
    "sensorLogs": Array<SensorSeriesInterface>;
    "eventLogs": Array<EventSeriesInterface>;
    "abnormalLogs": Array<EventSeriesInterface>;
    "motionLogs": Array<EventSeriesInterface>;
}

export interface SensorSeriesInterface {
    "sensorType": string;
    "vendorId": string;     //homd_id/uid/channelID
    "channelId": string;
    "updatedTimeStamp": string;
    "lastSensorValue": number
    "dataUnit": string;
    "valueScale": number;
    "sensorSeries": Array<SensorDataType>
}

export type SensorDataType = {
    "sensorVal":number;
    "timeStamp": number;

}

export interface EventSeriesInterface {
    "eventType": string;
    "vendorId": string;
    "channelId": string;
    "updatedTimeStamp": string;
    "lastEventVal"?: string;
    "eventSeries": Array<EventDataType>;
}

export type EventDataType = {
    "eventVal"?: string|number;
    "timeStamp": number;
}

