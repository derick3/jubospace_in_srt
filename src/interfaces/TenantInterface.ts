
type lastModeifiedUser = {
    "firstName": string;
    "lastName": string;
    "roles": Array<string>;
    "lineStatus": string;
    "_id": string;
    "sex": string;
    "employeeNumber": string;
    "staffStatus": string;
    "username": string;
    "birthday": string;
    "created": string;
    "provider": string;
    "displayName": string;
    "organization": string;
    "__v": number;
    "preference": preference
    "resignDate": boolean;
}

type preference = {
    "branch": string;
}

export interface TenantInfoClass {
    "lastName": string;
    "firstName": string;
    "sex": string;
    "nativeLanguage": Array<string>;
    "religion": Array<string>;
    "habits": Array<string>;
    "medications": Array<string>;
    "drugAllergies": Array<string>;
    "foodAllergies": Array<string>;
    "tube": Array<string>;
    "foodHandling": Array<string>;
    "respAids": Array<string>;
    "medicalHistory": Array<string>;
    "familyHealthProblems": Array<string>;
    "livingStatus": Array<string>;
    "occupyBed": string;
    "_id": string;
    "serviceType": string;
    "idNumber": string;
    "status": string;
    "createdDate": string;
    "lastModifiedDate": string;
    "organization": string;
    "roombed": string;
    "hospital": string;
    "birthday": string;
}

export interface NisResidentsObject {
        "nativeLanguage": Array<string>;
        "religion": Array<string>;
        "habits": Array<string>;
        "medications": Array<string>;
        "drugAllergies": Array<string>;
        "foodAllergies": Array<string>;
        "tube": Array<string>;
        "foodHandling": Array<string>;
        "respAids": Array<string>;
        "medicalHistory": Array<string>;
        "familyHealthProblems": Array<string>;
        "caseReferral": Array<string>;
        "livingStatus": Array<string>;
        "sourceOfFinance": Array<string>;
        "assistiveDevice": Array<string>;
        "socialWelfareStatus": Array<string>;
        "occupyBed": string;
        "powder": string;
        "_id": string;
        "sex": string;
        "checkInDate": string;
        "branch": string;
        "room": string;
        "bed": string;
        "serviceType": string;
        "lastName": string;
        "firstName": string;
        "idNumber": string;
        "status": string;
        "createdDate": string;
        "lastModifiedDate": string;
        "organization": string;
        "lastModifiedUser": lastModeifiedUser;
        "verificationCode": string;
        "__v": number;
        "roombed": string;
        "disabilityIDExpireDateScheduleLink": Boolean;
        "reapplicationDateScheduleLink": boolean;
}