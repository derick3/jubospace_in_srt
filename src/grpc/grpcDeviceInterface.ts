export interface DeviceMessage {
    transactionNo: number;
    NISPatientId: string;
    boxDeviceId: string;
    name: string;
    location: string;
    room: string;
    event?: string;
    state?: string;
    description?: string;
    message: string;
    occurredAt: string;

}

export interface DeviceMessageResp {
    transactionNo: number;
    serverResp: string;
}

export interface BedReport {
    transactionNo: number;
    NISPatientId: string;
    boxDeviceId: string;
    sleepStartTime: string;
    sleepEndTime: string;
    sleepLatency: number;
    sleepEffectiveness: number;
    turnOverCnt: number;
    notInBedCnt: number;
    notInBedTime: number;    
    mattressMac: number;    
    room: string;
    event: string;
    message: string;

}

export interface ActivityReport {
    "transactionNo": number; // sn
    "NISPatientId": string;// 5a9d013ea5012409ca68e1f5
    "boxDeviceId": string;// 52c2e0a4-7b33-4b0a-ac20-f93c44d38ed4
    "openCount": string;  // 開門次數
    "closeCount": number;// 關門次數
    "recordedAt": string;  // 記錄建立時間
    "location": string;    // 感應器位置
    "room": string;
    "event": string; //
    "message": string; //其他資訊
    
}

