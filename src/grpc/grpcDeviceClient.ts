'use strict';

const DEVICE_MESSAGE_PROTO_PATH = __dirname + '/protos/device_message.proto';
const DEVICE_REPORT_PROTO_PATH = __dirname + '/protos/device_report.proto';

const utils = require('./../utils/utilities')

const TRANS_MAX = 100000
const grpcClientIp = '127.0.0.1'
//const grpcClientIp = '192.168.69.83'
const grpcClientPort = '50052'

import {
    DeviceMessage,
    DeviceMessageResp,
    BedReport,
    ActivityReport,

} from './grpcDeviceInterface';

const grpc = require('grpc')
const protoLoader = require('@grpc/proto-loader');
const deviceMessageDefinition = protoLoader.loadSync(
    DEVICE_MESSAGE_PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    }
)

const deviceReportDefinition = protoLoader.loadSync(
    DEVICE_REPORT_PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    }
)

const deviceMessageProto = grpc.loadPackageDefinition(deviceMessageDefinition).device_message
const deviceReportProto = grpc.loadPackageDefinition(deviceReportDefinition).device_report
class JuboSpaceDeviceClient {
    uri: string;
    transactionNumber: number;
    deviceMessageHandler: any;
    deviceReportHandler: any;
    timerHandler: any;

    constructor(address: string|number, port: string|number){
        this.uri = address + ':' + port
        this.transactionNumber = 1;
        this.deviceMessageHandler = new deviceMessageProto.DeviceMessenger(this.uri,
            grpc.credentials.createInsecure()); 
        this.deviceReportHandler = new deviceReportProto.DeviceReport(this.uri,
            grpc.credentials.createInsecure());
        this.timerHandler = setInterval(this.serverTimerLoop.bind(this),2000)
    }

    private serverTimerLoop = () => {
        try{
            if(this.transactionNumber >= TRANS_MAX){
                this.transactionNumber = 1;
            } else if( true ) {
                // reset transactionNumber at 00:00:00 everyday
            }
        } catch (error) {
            console.log(`fn: serverTimerLoop ${error}`)
        }
    }

    public DoorWindwoGrpcUpdate = (activityReport: ActivityReport)=> {
        return new Promise((resolve,reject)=>{
            try{


                resolve(1)
            } catch(error){
                console.log(`fn: DoorWindwoGrpcUpdate ${error}`)
                reject(error)
            }
        })
    }

    public BedReportGrpcUpdate = (deviceReport: BedReport) => {
        return new Promise((resolve,reject)=>{
            try{
                console.log(deviceReport)
                deviceReport.transactionNo = this.transactionNumber
                this.deviceReportHandler.CreateBedReport(deviceReport,
                    (err,resp)=>{
                        if(err){reject(err)}
                        this.transactionNumber++
                        console.log('in BedReportGrpcUpdate resp')
                        console.log(resp)
                        resolve(resp)
                    })
            } catch(error){
                console.log(`fn: DeviceReportGrpcUpdate ${error}`)
                reject(error)
            }
        })
    }

    public DeviceStateGrpcUpdate = (deviceMessage: DeviceMessage) => {
        return new Promise((resolve,reject) => {
            try{
                deviceMessage.transactionNo = this.transactionNumber
                this.deviceMessageHandler.DeviceStateStub(deviceMessage,
                    (err,response) => {
                            if(err){reject(err)}
                            this.transactionNumber++;
                            //console.log("in DeviceStateGrpcUpdate resp")
                            //console.log(response)
                            resolve(response)
                        })
            } catch(error) {
                console.log(`fn:DeviceStateGrpcUpdate ${error}`)
                reject(error)
            }
        })
    }

    public DeviceEventGrpcUpdate = (deviceMessage: DeviceMessage) => {
        return new Promise((resolve,reject) => {
            try{
                deviceMessage.transactionNo = this.transactionNumber
                this.deviceMessageHandler.DeviceEventStub(deviceMessage,
                    (err,response) => {
                            if(err){reject(err)}
                            this.transactionNumber++
                            //console.log("in DeviceEventGrpcUpdate resp")
                            //console.log(response)
                            resolve(response)
                        })
            } catch(error) {
                console.log(`fn:DeviceEventGrpcUpdate ${error}`)
                reject(error)
            }
        })
    }    
}

const grpcDeviceClientHandler = new JuboSpaceDeviceClient(grpcClientIp,grpcClientPort)
exports.grpcDeviceClientHandler = grpcDeviceClientHandler

