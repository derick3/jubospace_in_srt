'use strict'

const toDate =(date) => {
    if(date === void 0 ) {
        return new Date(0)
    }
    if(isDateCheck(date)) {
        return date
    } else {
        return new Date(parseFloat(date.toString()))
    }
}

const isDateCheck = (date) => {
    return (date instanceof Date)
}

const isStrJsonFormat = (str:string):boolean => {
    try{
        JSON.parse(str)
    } catch(e){
        return false
    }
    return true
}

const formatDate = (date, format):string => {
    let d = toDate(date)
    return format
        .replace(/Y/gm, d.getFullYear().toString())
        .replace(/m/gm, ('0' + (d.getMonth() + 1)).substr(-2))
        .replace(/d/gm, ('0' + (d.getDate() + 0)).substr(-2))
        .replace(/H/gm, ('0' + (d.getHours() + 0)).substr(-2))
        .replace(/i/gm, ('0' + (d.getMinutes() + 0)).substr(-2))
        .replace(/s/gm, ('0' + (d.getSeconds() + 0)).substr(-2))
        .replace(/v/gm, ('0000' + (d.getMilliseconds() % 1000)).substr(-3));
}

const removeByteOrderMark = (str) => {
    // preserve newlines, etc - use valid JSON
    str = str.replace(/\\n/g, "\\n")
        // remove non-printable and other non-valid JSON chars
        .replace(/\\'/g, "\\'")
        .replace(/\'/g,"\"")
        .replace(/\\"/g, '\\"')
        .replace(/\\&/g, "\\&")
        .replace(/\\r/g, "\\r")
        .replace(/\\t/g, "\\t")
        .replace(/\\b/g, "\\b")
        .replace(/\\f/g, "\\f")
        // find ' ' and replace " " to keys with 3 or more char
        .replace(/(['"])?([a-zA-Z0-9_]{3,})(['"])?:/g, '"$2": ');

    str = str.replace(/[\u0000-\u0019]+/g, "");
    return str;
}

const waitPromise = (timeout: number) => {
    return new Promise((resolve,reject) => {
        try{
            setTimeout( ()=> {
                resolve()
            },timeout)
        } catch(error){
            console.log(`fn: waitPromise ${error}`)
            reject(error)
        }
    })
} 

const filterNewDataSeries = (array1:Array<any>, array2:Array<any>, objKeySelectArr:Array<string>, key:string) => {
    let result = array1.filter((obj1)=>{
        // filter out (!) items in array2
        return !array2.some((obj2)=>{
            return obj1[key] === obj2[key];          
        });
    }).map((obj)=>{
        // use reduce to make objects with only the required properties
        // and map to apply this to the filtered array as a whole
        return objKeySelectArr.reduce((newObjArr, name)=>{
            newObjArr[name] = obj[name];
            return newObjArr;
        }, {});
    });
    return result
}

exports.filterNewDataSeries = filterNewDataSeries
exports.waitPromise = waitPromise
exports.formatDate = formatDate
exports.removeByteOrderMark = removeByteOrderMark
exports.isStrJsonFormat = isStrJsonFormat

