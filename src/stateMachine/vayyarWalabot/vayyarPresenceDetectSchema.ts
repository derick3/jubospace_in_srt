//const grpcHandler = require('./vayyarGrpcAdapter')
const vayyarPresence = require('./vayyarGrpcAdapter')

module.exports = {
    initState: 'presenceDefault',
    presenceDefault: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },
        transitions: {
            isPresenceUpdate: {
                target: 'isPresence',
                action(message) {
                    console.log('presenceDefault => isPresence')
                    vayyarPresence.presenceDetectGrpcAdapter(message)
                }
            },
            noPresenceUpdate: {
                target: 'noPresence',
                action(message) {
                    console.log('presenceDefault => noPresence')
                }
            }                              
        }
    },    
    noPresence: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },
        transitions: {
            isPresenceUpdate: {
                target: 'isPresence',
                action(message) {
                    console.log('noPresence => isPresence') 
                    vayyarPresence.presenceDetectGrpcAdapter(message)
                }
            },
            noPresenceUpdate: {
                target: 'noPresence',
                action(message) {
                    console.log('noPresence => noPresence')  
                }
            }                                
        }
    },
    isPresence: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },
        transitions: {
            noPresenceUpdate: {
                target: 'noPresence',
                action(message) {
                    console.log('isPresence => noPresence')  
                    vayyarPresence.presenceCancelGrpcAdapter(message)
                }
            },
            isPresenceUpdate: {
                target: 'isPresence',
                action(message) {
                    console.log('isPresence => isPresence')  
                }
            }                                       
        }
    }
}