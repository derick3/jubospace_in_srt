'use strict'

const grpcHandler = require('./../../grpc/juboSpaceDeviceClient').grpcDeviceClientHandler

const vayyarApi = require('./vayyarGrpcAdapterApi')
const utils = require('./../../utils/utilities')

const fallDetectGrpcAdapter = async (message : any) => {
    try {
        console.log(message)
        let messageTemp = vayyarApi.vayyarFallAdapterLogic
                .find((x: { status: string; }) => x.status === message.fallReport.status).message
        
        if(messageTemp === undefined){messageTemp = '偵測系統初始'}

        await grpcHandler.DeviceEventGrpcUpdate({
            "transactionNo": 0,
            "NISPatientId": message.deviceInfo.patientId,
            "boxDeviceId": message.deviceInfo.deviceId,
            "name": "Vayyar Walabot Home",
            "location": message.deviceInfo.location.position,
            "room": message.deviceInfo.location.roomNum,
            "event": message.fallReport.status,
            "message": messageTemp,
            //"occurredAt": utils.formatDate(message.fallReport.statusUpdateTimestamp,"Y-m-d H:i:s")
            "occurredAt": new Date(message.fallReport.statusUpdateTimestamp).toISOString()
        })

    } catch (error) {
        console.log(`fn: processFallDetectGrpc ${error}`)
    }
 }
 
 const fallCancelGrpcAdapter = async (message : any) => {
    try {
        console.log(message)
        let messageTemp = vayyarApi.vayyarFallAdapterLogic
                .find((x: { status: string; }) => x.status === message.fallReport.status).message
        
        if(messageTemp === undefined){messageTemp = '偵測系統初始'}

        await grpcHandler.DeviceEventGrpcUpdate({
            "transactionNo": 0,
            "NISPatientId": message.deviceInfo.patientId,
            "boxDeviceId": message.deviceInfo.deviceId,
            "name": "Vayyar Walabot Home",
            "location": message.deviceInfo.location.position,
            "room": message.deviceInfo.location.roomNum,
            "event": message.fallReport.status,
            "message":messageTemp,
            //"occurredAt": utils.formatDate(message.fallReport.statusUpdateTimestamp,"Y-m-d H:i:s")
            "occurredAt": new Date(message.fallReport.statusUpdateTimestamp).toISOString()
        })

    } catch (error) {
        console.log(`fn: fallCancelGrpcAdapter ${error}`)
    }
 }

 const presenceDetectGrpcAdapter = async (message : any) => {
     try {
        console.log(message)
        await grpcHandler.DeviceEventGrpcUpdate({
            "transactionNo": 0,
            "NISPatientId": message.deviceInfo.patientId,
            "boxDeviceId": message.deviceInfo.deviceId,
            "name": "Vayyar Walabot Home",
            "location": message.deviceInfo.location.position,
            "room": message.deviceInfo.location.roomNum,
            "event": "Presence Detected",
            "message":"偵測活動事件",
            //"occurredAt": utils.formatDate(message.presenceReport.timestamp,"Y-m-d H:i:s")
            "occurredAt": new Date(message.presenceReport.timestamp).toISOString()
        })

     } catch (error) {
        console.log(`fn:presenceDetectGrpcAdapter ${error}`)
     }
}

const presenceCancelGrpcAdapter = async (message : any) => {
    try {
        console.log(message)
        await grpcHandler.DeviceEventGrpcUpdate({
            "transactionNo": 0,
            "NISPatientId": message.deviceInfo.patientId,
            "boxDeviceId": message.deviceInfo.deviceId,
            "name": "Vayyar Walabot Home",
            "location": message.deviceInfo.location.position,
            "room": message.deviceInfo.location.roomNum,
            "event": "Presence Canceled",
            "message":"解除活動事件",
            //"occurredAt": utils.formatDate(message.presenceReport.timestamp,"Y-m-d H:i:s")
            "occurredAt": new Date(message.presenceReport.timestamp).toISOString()
        })

    } catch (error) {
        console.log(`fn:presenceCancelGrpcAdapter`)
    }
}

 exports.fallDetectGrpcAdapter = fallDetectGrpcAdapter;
 exports.fallCancelGrpcAdapter = fallCancelGrpcAdapter;

 exports.presenceDetectGrpcAdapter = presenceDetectGrpcAdapter;
 exports.presenceCancelGrpcAdapter = presenceCancelGrpcAdapter;