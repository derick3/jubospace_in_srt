'use strict'

const vayyarFallDetect = require('./vayyarGrpcAdapter')

module.exports = {
    initState: 'fallDefault',
    fallDefault: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },
        transitions: {
            fall_detected: {
                target: 'fall_detected',
                action(message) {
                    message["message"] = "感應跌跤狀態"
                    vayyarFallDetect.fallDetectGrpcAdapter(message)
                    console.log('fallDefault->fall_detected')
                }
            },
            fall_exit: {
                target: 'fall_exit',
                action(message) {
                    console.log('fallDefault->fall_exit')
                }
            },
            canceled: {
                target: 'canceled',
                action(message) {
                    console.log('fallDefault->canceled')
                    if(message.fallReport.isSimulated === true){
                        message["message"] = "解除跌跤狀態 - 模擬"
                    } else {
                        message["message"] = "解除跌跤狀態"
                    }
                    vayyarFallDetect.fallCancelGrpcAdapter(message)
                }
            }                                          
        }
    },    
    fall_exit: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },
        transitions: {
            fall_exit: {
                target: 'fall_exit',
                action(message) {
                    console.log('fall_exit->fall_exit')
                }
            },               
            fall_detected: {
                target: 'fall_detected',
                action(message) {
                    message["message"] = "感應跌跤狀態"
                    vayyarFallDetect.fallDetectGrpcAdapter(message)
                    console.log('fall_exit->fall_detected')
                }
            },
            canceled: {
                target: 'canceled',
                action(message) {
                    console.log('fall_exit->canceled')
                }
            }                  
        }
    },
    fall_detected: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },
        transitions: {
            fall_detected: {
                target: 'fall_detected',
                action(message) {
                    console.log('fall_detected->fall_detected')
                }
            },            
            fall_confirmed: {
                target: 'fall_confirmed',
                action(message) {
                    message["message"] = "跌倒"
                    console.log('fall_detected->fall_confirmed')
                    vayyarFallDetect.fallDetectGrpcAdapter(message)
                }
            },
            calling: {
                target: 'calling',
                action(message) {
                    console.log('fall_detected->calling')
                }
            },fall_exit: {            
                target: 'fall_exit',
                action(message) {
                    console.log('fall_detected->fall_exit')
                }
            },canceled: {            
                target: 'canceled',
                action(message) {
                    console.log('fall_detected->canceled')
                }
            }                                     
        }
    },
    fall_confirmed: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },
        transitions: {
            fall_confirmed: {
                target: 'fall_confirmed',
                action(message) {
                    console.log('fall_confirmed->fall_confirmed')
                }
            },            
            calling: {
                target: 'calling',
                action(message) {
                    message["message"] = "跌倒通報"
                    console.log('fall_confirmed->calling')
                    vayyarFallDetect.fallDetectGrpcAdapter(message)
                }
            },
            canceled: {
                target: 'canceled',
                action(message) {
                    message["message"] = "解除跌倒警戒"
                    console.log('fall_confirmed->canceled')
                    vayyarFallDetect.fallCancelGrpcAdapter(message)
                }
            },
            fall_exit: {
                target: 'fall_exit',
                action(message) {
                    message["message"] = "解除跌倒警戒"
                    console.log('fall_confirmed->fall_exit')
                    vayyarFallDetect.fallCancelGrpcAdapter(message)
                }
            }                                       
        }
    },
    calling: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },      
        transitions: {
            calling: {
                target: 'calling',
                action(message) {}
            },              
            canceled: {
                target: 'canceled',
                action(message) {
                    message["message"] = "解除跌倒警戒"
                    vayyarFallDetect.fallCancelGrpcAdapter(message)
                }
            },
            fall_exit: {
                target: 'fall_exit',
                action(message) {
                    message["message"] = "解除跌倒警戒"
                    vayyarFallDetect.fallCancelGrpcAdapter(message)
                }
            }                                       
        }
    },
    canceled: {
        actions: {
            onEnter(message) {},
            onExit(message) {},
        },      
        transitions: {
            canceled: {
                target: 'canceled',
                action(message) {
                    console.log('fallStatus canceled -> canceled ')
                }
            },              
            fall_detected: {
                target: 'fall_detected',
                action(message) {
                    message["message"] = "感應跌跤狀態"
                    vayyarFallDetect.fallDetectGrpcAdapter(message)
                }
            },
            fall_exit: {
                target: 'fall_exit',
                action(message) {
                    console.log('fallStatus canceled -> fall_exit ')
                }
            }                                       
        }        
    }    
}
