'use strict'

const fsm = require('./../statemachine')
const walabotPuppet = require('./walabotv1Puppet').WalabotV1
const fallDetectDefinition = require('./vayyarFallDetectStateSchema')
const presenceDetectDefinition = require('./vayyarPresenceDetectSchema')
const controllerDefinition = require('./vayyarControllerSchema')

import { VayyarPresenceMessage, VayyarFallMessage } from "./vayyarInterface"
import { vayyarDeviceDbInterface, JuboBoxInfo } from './../../interfaces/nanoCouchInterface'
import type { GenericSpaceInfo } from './../../interfaces/genericInterface'

const JuboDeviceDb = require('./../../nanoCouchDb/juboSpaceDeviceDb')
const deviceDb = new JuboDeviceDb()
interface VayyarController {

}

class VayyarController implements VayyarController {

   fallDetector : VayyarFallDetect 
   presenceDetector : VayyarPresenceDetect 
    
   deviceState: string
   spaceInfo: GenericSpaceInfo

   initController : any
   timerHandler : any
   localCounter: number
   fsMachine : any

   deviceDb: any
   juboBoxInfo: JuboBoxInfo
   deviceInfo: vayyarDeviceDbInterface

   private walabotHandler = new walabotPuppet()

   constructor (spaceInfo : GenericSpaceInfo, deviceDbHandler:any) {
      
      this.spaceInfo = spaceInfo
      this.deviceDb = deviceDbHandler
      this.deviceDb.getVayyarDeviceList().then((resp)=>{
            console.log(resp)
            this.deviceInfo = resp[0]
            
            this.fallDetector = new VayyarFallDetect(this.spaceInfo,this.deviceInfo)
            this.presenceDetector = new VayyarPresenceDetect(this.spaceInfo,this.deviceInfo)
         
      })
      this.deviceDb.getJuboBoxInfo().then((resp)=>{
         this.juboBoxInfo = resp
         console.log(this.juboBoxInfo)
      })
      this.fsMachine = fsm.createStateMachine(controllerDefinition)
      this.deviceState = 'waitDevice'
      this.localCounter = 0

      this.initWalabot();
   }
   private initWalabot () {
      let initializer = new Promise((resolve,reject) => {
         try{
            resolve(this.walabotHandler.puppetInitWalabot())
         } catch(error){
            reject(error)
         } 
      })

      initializer.then((result:string)=>{
         if(this.deviceInfo.vendorId === result){
            this.deviceState = this.fsMachine.transitions('waitDevice','deviceFound')
         } else {
            console.log('vayyar device id not the same in db')
            console.log(result)
            console.log(this.deviceInfo.vendorId)
            this.addNewVayyarDeviceToDb(result).then((resp)=>{
               console.log(resp)
            })
            
         }
      })
      this.timerHandler = setInterval(this.timeMethods.bind(this),5000)
   }

   private timeMethods () : void {
      try{
         if(this.deviceState != undefined){
            console.log('Walabot ===> ' + this.deviceState)
            //console.log(this.deviceInfo)
            switch(this.deviceState){
               case 'waitDevice':
                  //console.log('waitDevice State')
                  this.scrapId()
               break;
      
               case 'deviceConnect':
                  this.scrapState()
               break;
      
               case 'stateAquired':
                  this.scrapPresence()
                  this.scrapFall() 
                  this.localCounter++
                  //console.log("Counting Idling: "+this.localCounter)
                  if(this.localCounter >= 60){
                     
                     //Walabot Idle without message Too Long
                     //Handle Reboot Walabot Externally
                  }   
                     
               break;
               
               case 'systemOperational':
                  this.scrapFall()
                  this.scrapPresence()
               break;
      
               case 'systemError':
      
               break;
      
               default:
                  this.deviceState = 'deviceConnect'
               break;
            }
         }
      } catch(error) {
         console.log(`fn:timeMethods ${error}`)
      }
   }

   private addNewVayyarDeviceToDb = (newId:string) =>{
      return new Promise((resolve,reject)=>{
         try{
            let newWalabot : vayyarDeviceDbInterface = {
               "deviceId": newId,
               "vendorId": newId,
               "deviceName": "Vayyar Walabot Home V1",
               "patientId": "",
               "location": {
                  "facility": "",
                  "roomNum": "",
                  "position": ""
               },
               "createdTime": new Date(Date.now()).toISOString(),
               "updatedTime": new Date(Date.now()).toISOString(),
               "lastBeatTime": new Date(Date.now()).toISOString()
            }


            resolve()
         } catch(error){
            console.log(`fn:addNewVayyarDeviceToDb`)
            reject(error)
         }
      })

   }

   private scrapId = () => {
      try{
        this.walabotHandler.getId()
         .then((resp) => {
            if(this.deviceInfo.deviceId === resp){
               this.deviceState = this.fsMachine.transitions('waitDevice','deviceFound')
            } else {
               console.log('vayyar device id not the same in db')
               console.log(resp)
               console.log(this.deviceInfo.vendorId)
               this.addNewVayyarDeviceToDb(resp).then((resp)=>{
                  console.log(resp)
               })             
            }
         })
      } catch(error) {
         if(error.message.includes('TimeoutError')){
           
         } else {
            console.log(`fn: scrapId ${error}`)
         }
      }
   }

   private scrapState = () => {
      try {
         this.walabotHandler.getState()
         .then((jsonResp)=> {
            if(jsonResp.State.status === 'monitoring'){
               this.deviceState = this.fsMachine.transitions('deviceConnect','monitorUp')
            } else {
               console.log(jsonResp.State.status)
            }
         })
      } catch (error) {
         if(error.message.includes('TimeoutError')){
            console.log(error)
         } else {
            console.log(`fn: scrapState ${error}`)
         }
      }   
   }

   private scrapPresence = () => {
      try {
         this.walabotHandler.getPresenceStatus()
         .then((jsonResp)=>{
            let presenceStatus = jsonResp.Presence
            if(presenceStatus.hasOwnProperty('presenceDetected')){
               if(this.deviceState === 'stateAquired'){
                  this.deviceState = this.fsMachine.transitions('stateAquired','presenceUpdate')
               }
               
               if(presenceStatus.presenceDetected){     //=true          
                  this.presenceDetector.updatePresenceStatus(presenceStatus)
               } else {                                  //=false
                  this.presenceDetector.updatePresenceStatus(presenceStatus)
               }
            }
         })
      } catch (error) {
         if(error.message.includes('TimeoutError')){
            console.log(`fn: scrapPresence ${error}`)
         }
      }   
   }

   private scrapFall = () => {
      try {
         this.walabotHandler.getFallStatus()
         .then((jsonResp)=>{
            //console.log(jsonResp)
            let fallStatus = jsonResp.Fall
            //console.log(fallStatus)
            if(fallStatus.hasOwnProperty('status')){
               this.fallDetector.updateFallStatus(fallStatus)
            }

         })
      } catch (error) {
         if(error.message.includes('TimeoutError')){
            console.log(`fn: scrapFall ${error}`)
         }
      }   
   }   
}

class VayyarFallDetect {
   fsMachine : any
   fallStatus : string;
   initState : any
   spaceInfo : GenericSpaceInfo
   deviceInfo : vayyarDeviceDbInterface
   constructor(spaceInfo : GenericSpaceInfo,deviceInfo:vayyarDeviceDbInterface){
      
      this.fsMachine = fsm.createStateMachine(fallDetectDefinition)
      this.spaceInfo = spaceInfo
      this.deviceInfo = deviceInfo 
      console.log(deviceInfo)
      this.initState = () => {
         this.fallStatus = this.fsMachine.value;
      }
      this.initState();
   } 

   public updateFallStatus = ( fallJson : VayyarFallMessage ) => {
      try{
         //console.log(fallJson)
         //console.log(this.fallStatus)
         let message = {deviceInfo: this.deviceInfo, fallReport: fallJson}
         if(this.fallStatus != fallJson.status){
            switch(fallJson.status){
               case 'fall_exit':
                  this.fallStatus = this.fsMachine.transitions(this.fallStatus,'fall_exit',message)
                  
               break;
   
               case 'fall_detected':
                  this.fallStatus = this.fsMachine.transitions(this.fallStatus,'fall_detected',message)
               break;
   
               case 'fall_confirmed':
                  this.fallStatus = this.fsMachine.transitions(this.fallStatus,'fall_confirmed',message)
                  
               break;
   
               case 'calling':
                  this.fallStatus = this.fsMachine.transitions(this.fallStatus,'calling',message)
               break;
   
               case 'canceled':
                  this.fallStatus = this.fsMachine.transitions(this.fallStatus,'canceled',message)
               break;
   
               default:
                  console.log('i am falling into rabbit hole')
               break;
            }
         }
      } catch (error) {
         console.log(`fn:updateFallStatus ${error}`)
      }
   }
}

class VayyarPresenceDetect {
   fsMachine: any
   presenceStatus: string
   initState: any
   spaceInfo : GenericSpaceInfo
   deviceInfo : any
   deviceLocation: string
   accumulatedNoPresence: number
   constructor (spaceInfo : GenericSpaceInfo,deviceInfo:vayyarDeviceDbInterface) {
      
      this.fsMachine = fsm.createStateMachine(presenceDetectDefinition)
      this.spaceInfo = spaceInfo
      this.deviceInfo = deviceInfo
      console.log(deviceInfo)
      this.initState = () => {
         this.presenceStatus = this.fsMachine.value;
      }
      this.initState();      
   }

   public updatePresenceStatus = (presenceJSON: VayyarPresenceMessage) => {
      try {
         let message = {deviceInfo: this.deviceInfo, presenceReport: presenceJSON}
         if(presenceJSON.presenceDetected){
            this.presenceStatus = this.fsMachine.transitions(this.presenceStatus,'isPresenceUpdate',message)
         } else {
            this.presenceStatus = this.fsMachine.transitions(this.presenceStatus,'noPresenceUpdate',message)
         }
      } catch(error) {
         console.log(`fn:updatePresenceStatus ${error}`)
      }
   }
}

module.exports = VayyarController;
