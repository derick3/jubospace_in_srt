'use strict'

const grpcHandler = require('./../../grpc/juboSpaceDeviceClient').grpcDeviceClientHandler
import { MwgSetting } from './mwgInterface'
const mwgApi = require('./mwgGrpcAdapterApi')
const utils = require('./../../utils/utilities')


import { GenericSpaceInfo } from './../../interfaces/genericInterface'
import { mwgMattressDbInterface } from './../../interfaces/nanoCouchInterface'
import { MwgSleepInfo } from './../../grpc/grpcOldInterface'

const bedActivityGrpcAdapter = async (message:any, logic : any  ) => {
    try {
        if(logic.bedType === undefined){throw new Error("Undefined Bed Source calling bedActivityGrpcAdapter")}
        else if(logic.bedType === 'mwg'){
            //console.log(message)
            let bedState = mwgApi.mwgAdapterLogic
                            .find((x: { bedState: number; }) => x.bedState === message.bedPacket.bedState)
            console.log(bedState)

            await grpcHandler.DeviceEventGrpcUpdate({
                "transactionNo": 0,
                "NISPatientId": message.deviceInfo.patientId,
                "boxDeviceId": message.deviceInfo.deviceId,
                "name": "MWG Bed Mattress",
                "location": message.deviceInfo.location.position,
                "room": message.deviceInfo.location.roomNum,
                "event": bedState.stateType,
                "message": bedState.stateMessage,
                //"occurredAt": utils.formatDate(bedPacket.timestamp,"Y-m-d H:i:s")
                "occurredAt": new Date(message.bedPacket.timestamp).toISOString()
            })
        }
    } catch(error){
        console.log(`bedActivityGrpcAdapter ${error}`)
    }
}

const bedSystemGrpcAdapter = async (message:any, logic : any, ) => {
    try{

        if(logic.bedType === undefined){throw new Error("Undefined Bed Source calling bedSystemGrpcAdapter")}
        else if(logic.bedType === 'mwg'){

            let bedState = mwgApi.mwgAdapterLogic
                            .find((x: { bedState: number; }) => x.bedState === message.bedPacket.bedState)
            console.log(bedState) 

            await grpcHandler.DeviceEventGrpcUpdate({
                "transactionNo": 0,
                "NISPatientId": message.deviceInfo.patientId,
                "boxDeviceId": message.deviceInfo.deviceId,
                "name": "MWG Bed Mattress",
                "location": message.deviceInfo.location.position,
                "room": message.deviceInfo.location.roomNum,
                "event": bedState.stateType,
                "message": bedState.stateMessage,
                //"occurredAt": utils.formatDate(bedPacket.timestamp,"Y-m-d H:i:s")
                "occurredAt": new Date(message.bedPacket.timestamp).toISOString()
            })            
        }
    } catch (error) {
        console.log(`bedSystemGrpcAdapter ${error}`)
    }
}

const bedAlertGrpcAdapter = async (message:any , logic : any) => {
    try{
        //console.log(bedPacket)
        if(logic.bedType === undefined) { throw new Error("Undefined Bed Source calling bedAlertGrpcAdapter")}
        else if (logic.bedType === 'mwg'){
            //console.log(spaceInfo)
            let bedState = mwgApi.mwgAdapterLogic
                .find((x: { bedState: number; }) => x.bedState === message.bedPacket.bedState)
            console.log(bedState)

            await grpcHandler.DeviceEventGrpcUpdate({
                "transactionNo": 0,
                "NISPatientId": message.deviceInfo.patientId,
                "boxDeviceId": message.deviceInfo.deviceId,
                "name": "MWG Bed Mattress",
                "location": message.deviceInfo.location.position,
                "room": message.deviceInfo.location.roomNum,
                "event": bedState.stateType,
                "message": bedState.stateMessage,
                //"occurredAt": utils.formatDate(bedPacket.timestamp,"Y-m-d H:i:s")
                "occurredAt": new Date(message.bedPacket.timestamp).toISOString()
            })
        }
    } catch(error){
        console.log(`bedAlertGrpcAdapter ${error}`)
    }
}

interface SleepInfoIncoming {
    "deviceInfo": mwgMattressDbInterface;
    "bedPacket": MwgSleepInfo;
}
const sleepInfoGrpcAdapter = async (message : SleepInfoIncoming, logic : any) => {
    try {

        if(logic.bedType === undefined) {throw new Error ("Undefined Bed Source calling sleepInfoGrpcAdapter")}
        else if(logic.bedType === 'mwg'){

            await grpcHandler.BedReportGrpcUpdate({
                "transactionNo": 0,
                "NISPatientId": message.deviceInfo.patientId,
                "boxDeviceId": message.deviceInfo.deviceId,
                "sleepStartTime": new Date(message.bedPacket.sleepStartTime).toISOString(),
                "sleepEndTime": new Date(message.bedPacket.sleepEndTime).toISOString(),   
                "sleepLatency": message.bedPacket.sleepLatency,
                "sleepEffectiveness": message.bedPacket.sleepEffectiveness,
                "turnOverCnt": message.bedPacket.turnOverCnt,
                "notInBedCnt": message.bedPacket.notInBedCnt,
                "notInBedTime": message.bedPacket.totalNotInBedTime,   
                "mattressMac": parseInt(message.bedPacket.mattressMac,16),                                        
                "room": message.deviceInfo.location.roomNum,
                "event": "Sleep Info Report",
                "message": "睡眠資訊報告"
    
            })
        }
    } catch( error){
        console.log(`sleepInfoGrpcAdapter ${error}`)
    }
}

const bedSettingGrpcAdapter = async (spaceInfo: GenericSpaceInfo, bedPacket: any, source: string) => {
    try{
        //console.log(bedPacket)
        //await gRpc.BedSettingUpdate(bedPacket)

    } catch(error){
        console.log(`bedSettingGrpcAdapter ${error}`)
    }
}

exports.bedActivityGrpcAdapter = bedActivityGrpcAdapter
exports.bedSystemGrpcAdapter = bedSystemGrpcAdapter
exports.bedAlertGrpcAdapter = bedAlertGrpcAdapter

exports.sleepInfoGrpcAdapter = sleepInfoGrpcAdapter
exports.bedSettingGrpcAdapter = bedSettingGrpcAdapter