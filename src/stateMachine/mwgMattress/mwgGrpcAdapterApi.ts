'use strict'

module.exports.mwgAdapterLogic = [
    {
        bedState:1,
        stateMessage:'完全離床',
        stateType:'Bed Activity'
    },
    {
        bedState:4,
        stateMessage:'臥床',
        stateType:'Bed Activity'
    },
    {
        bedState:12,
        stateMessage:'起身',
        stateType:'Bed Activity'
    },
    {
        bedState:13,
        stateMessage:'進床',
        stateType:'Bed Activity'
    },
    {
        bedState:14,
        stateMessage:'坐在床緣',
        stateType:'Bed Activity'
    },
    {
        bedState:15,
        stateMessage:'側躺',
        stateType:'Bed Activity'
    },
    {
        bedState:203,
        stateMessage:'床墊接頭脫落',
        stateType:'Bed System'
    },
    {
        bedState:206,
        stateMessage:'床墊離線',
        stateType:'Bed System'
    },
    {
        bedState:208,
        stateMessage:'床墊上線',
        stateType:'Bed System'
    },
    {
        bedState:217,
        stateMessage:'久臥',
        stateType:'Bed Alert'
    },            
    {
        bedState:218,
        stateMessage:'離床過久',
        stateType:'Bed Alert'
    }

]