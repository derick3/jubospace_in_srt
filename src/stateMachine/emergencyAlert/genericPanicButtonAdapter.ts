'use strict'

const panicButtonHandler = require('./../../grpc/juboSpaceDeviceClient').grpcDeviceClientHandler
const utils = require('./../../utils/utilities')
import { iPadDeviceDbInterface } from './../../interfaces/nanoCouchInterface'
import { GenericSystemInfo } from './../../interfaces/genericInterface'

const panicButtonPressedGrpcAdapter = async (deviceInterface: iPadDeviceDbInterface | any,) => {
    try{
        //console.log(deviceInterface)
        return await panicButtonHandler.DeviceEventGrpcUpdate({
            "NISPatientId": deviceInterface.patientId,
            "boxDeviceId": deviceInterface.deviceId,
            "name": deviceInterface.deviceName,
            //"location": (deviceInterface.location.position === '')? 'bed:'+ deviceInterface.location.bedNum : 'Default Position',
            "location": 'bed:' + deviceInterface.location.bedNum,
            "room": deviceInterface.location.roomNum,
            "event": 'Panic Button Pressed',
            "message": '緊急按鈕觸發',
            //"occurredAt": utils.formatDate(new Date().getTime(),"Y-m-d H:i:s")
            "occurredAt": new Date(Date.now()).toISOString()
        })
    } catch (error) {
        console.log(`fn: panicButtonPressedGrpcAdapter ${error}`)
    }
}

const panicButtonClearedGrpcAdapter = async (deviceInterface: iPadDeviceDbInterface | any) => {
    try{
        console.log(deviceInterface)
        return await panicButtonHandler.DeviceEventGrpcUpdate({
            "NISPatientId": deviceInterface.patientId,
            "boxDeviceId": deviceInterface.deviceId,
            "name": deviceInterface.deviceName,
            //"location": (deviceInterface.location.position === '')? 'bed:'+ deviceInterface.location.bedNum : 'Default Position',
            "location": 'bed:' + deviceInterface.location.bedNum,
            "room": deviceInterface.location.roomNum,
            "event": 'Panic Button Cleared',
            "message": '緊急按鈕解除',
            //"occurredAt": utils.formatDate(new Date().getTime(),"Y-m-d H:i:s")
            "occurredAt": new Date(Date.now()).toISOString()
        })
    } catch (error) {
        console.log(`fn: panicButtonClearedGrpcAdapter ${error}`)
    }
}

exports.panicButtonPressedGrpcAdapter = panicButtonPressedGrpcAdapter
exports.panicButtonClearedGrpcAdapter = panicButtonClearedGrpcAdapter

