"use strict";
const EventEmitter = require('events');
const panicButtonDefinition = require('./panicButtonStateSchema')
const panicButtonAdapter = require('./genericPanicButtonAdapter')

const fsm = require('./../statemachine')
class PanicButton extends EventEmitter {
    uid: number;
    chid: number;
    group: string;
    location: string;
    homeId: string;
    productId: string;
    functype: string;
    name: string;
    roomNum: number;
    userDefineName: string;
    powerLevel: number;
    previousPanicTime: number;
    panicStatus: string;
    powerStatus: string;
    PanicMachine: any;
    initState: any;
 
    constructor(roomNum:number,panicButton:any){
        super()
        this.uid = panicButton.uid;
        this.chid = panicButton.chid;
        this.group = panicButton.group;
        this.location = panicButton.location;
        this.homeId = panicButton.homeId;
        this.productId = panicButton.productId;
        this.functype = panicButton.functype;
        this.name = panicButton.name;
        this.roomNum = roomNum;
        this.userDefineName = panicButton.userDefineName;
        this.powerLevel = 100;
        this.previousPanicTime = 0;
        this.panicStatus = 'noPanic';
        this.powerStatus = 'normal';

        this.PanicMachine = fsm.createStateMachine(panicButtonDefinition)

        this.initState = () => {
            this.panicStatus = this.PanicMachine.value;
        }

        this.initState();
    }

    public ButtonPressed = (event) => {
        let resp = ''
        if(this.PanicMachine.value==='noPanic'){
            //console.log('ButtonPressed')
            // console.log(event)
            switch(event.funcType){
                case 'Meter Switch':
               
                    this.panicStatus = this.PanicMachine.transitions('noPanic', 'buttonPanicPressed',event)
                    procIsPanicGrpc(event,this);
                    resp = '/ack=panic-button-pressed';
                    // JuboWebSocket.wssBroadcast(resp);                      
                break;
                case 'Switch':
                    this.panicStatus = this.PanicMachine.transitions('noPanic', 'buttonPanicPressed',event)
                    procIsPanicGrpc(event,this);
                    resp = '/ack=panic-button-pressed';
                    // JuboWebSocket.wssBroadcast(resp);                 
                    
                break;
                case 'iPadPanicBtn':
                    this.panicStatus = this.PanicMachine.transitions('noPanic', 'iPadPanicPressed',event)
                    procIsPanicGrpc(event,this);                    
                    resp = '/ack=panic-button-pressed';
                    // JuboWebSocket.wssBroadcast(resp); 
                break;
    
                default:
    
                break;
            }
        }
    }

    public ButtonCleared = (event) => {
        let resp = ''
        if(this.PanicMachine.value ==='isPanic'){
            // console.log(event)
            switch(event.funcType){            
                case 'Meter Switch':
                    this.panicStatus = this.PanicMachine.transitions('isPanic', 'buttonPanicCleared',event)
                    procNoPanicGrpc(event,this);
                    resp = '/ack=panic-button-cleared';
                   // JuboWebSocket.wssBroadcast(resp); 
                break;
    
                case 'Switch':
                    this.panicStatus = this.PanicMachine.transitions('isPanic', 'buttonPanicCleared',event)
                    procNoPanicGrpc(event,this);
                    resp = '/ack=panic-button-cleared';
                   // JuboWebSocket.wssBroadcast(resp); 
                break;
    
                case 'iPadPanicBtn':
                    this.panicStatus = this.PanicMachine.transitions('isPanic', 'iPadPanicCleared',event)
                    procNoPanicGrpc(event,this);
                    resp = '/ack=panic-button-cleared';
                  //  JuboWebSocket.wssBroadcast(resp); 
                break;
                
                case 'dashBoard':
                    this.panicStatus = this.PanicMachine.transitions('isPanic', 'dashBoardPanicCleared',event)
                    procNoPanicGrpc(event,this);
                    resp = '/ack=panic-button-cleared';
                    // JuboWebSocket.wssBroadcast(resp);                     
                break;
    
                case 'timeout':
                    this.panicStatus = this.PanicMachine.transitions('isPanic', 'timeoutPanicCleared',event)
                break;
    
                default:
    
                break;
    
            }
        }
    }

    public PanicTimeout = (event) => {

    }

    public updateLocalIpad = (event) => {
        // console.log('updateLocalIpad')
    }
}

const reconnectGrpcServer = () => {

    console.log('reconnectGrpcServer Unhandled')
}
const procIsPanicGrpc = async (eventLog,self) => {
    try{
        let message = {...eventLog,...self}        
        await panicButtonAdapter.PanicButtonStatusUpdate(message)

    } catch(error){
        console.log('fn: procIsPanicGrpc: '+error)
    }
}

const procNoPanicGrpc = async (eventLog,self) => {
    try{
        let message = {...eventLog,...self}        
        await panicButtonAdapter.PanicButtonStatusUpdate(message)    
    } catch(error){
        console.log('fn: procNoPanicGrpc: '+error)
    }
}

module.exports = PanicButton;