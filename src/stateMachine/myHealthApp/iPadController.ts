'use strict'
export {}
//const fsm = require('./../../stateMachine/statemachine')
//const panicButtonController = require('./../emergencyAlert/panicButtonFsm')
const iPadSysGrpcHandler = require('./deviceSystemGrpcAdapter')
const iPadPanicGrpcHandler = require('./../emergencyAlert/genericPanicButtonAdapter')
const philioSystemHandler = require('./../philioZwave/systemControl/philioSystemControl')
const EventEmitter = require('events')
const utils = require('./../../utils/utilities')

import { WsConnectToken } from './interfaces/myHealthAppInterface'
import { PhilioGatewayObject } from './../philioZwave/interfaces/philioInterface'
import { iPadDeviceDbInterface } from './../../interfaces/nanoCouchInterface'
import { GenericSystemInfo } from './../../interfaces/genericInterface'


const IPAD_BATTERY_LOW_LEVEL = 25

class iPadClientController extends EventEmitter {
    
    initController: any
    systemInfo: GenericSystemInfo
    wsHandler: any
    philio: PhilioGatewayObject
    eventHandle: any
    constructor (connectToken:WsConnectToken, ws: any, philio: any) {
        super()
        this.initController = this.initIpadAssociate()
        this.systemInfo = {
            "batteryLevel": 0
        }
        this.wsHandler = ws
        this.philio = philio
    }

    private initIpadAssociate = () => {
        console.log('initIpadAssociate')
    }

    public handleIncomingMessage = async (iPad: iPadDeviceDbInterface, msg:string):Promise<any> => {
        try{
            //console.log(msg)
            if(msg.includes('/lightmode')){
                await this.handleLightControlRequest(iPad,msg)
            } else {
                let grpcResp:any
                let head = msg.split("=")[0]
                let body = msg.split("=")[1]
                let deviceId = head.split('#')[0].split('/')[1]
                let cmd = head.split('#')[1]
                if(deviceId === iPad.deviceId){
                    switch(cmd) {
                        case 'panicButton':
                            grpcResp =  await this.handlePanicButtonRequest(iPad,body)
                            if(grpcResp){
                                //console.log(grpcResp)
                                if(grpcResp.serverResp === 'OK' || 
                                grpcResp.serverResp ===  'resp DeviceEvent from server'){
                                    this.wsHandler.send('/'+ deviceId + '#'+ cmd + '='+body+'_success')
                                    return 'handleIncomingDone'
                                }
                            } else {
                                throw new Error('Unknown Grpc Response')
                            }    
                        break;
    
                        case 'lightControl':
                            grpcResp = await this.handleLightControlRequest(iPad,body)
                            if(grpcResp){
                                //console.log(grpcResp)
                                if(grpcResp.serverResp === 'OK' ||
                                    grpcResp.serverResp === 'resp DeviceEvent from server'){
                                    this.wsHandler.send('/'+ deviceId + '#'+ cmd + '='+body+'_success')
                                    return 'handleIncomingDone'
                                }
                            } else {
                                throw new Error('Unknown Grpc Response')
                            }                            
                        break;
    
                        case 'systemInfo':
                            grpcResp = await this.handleSystemInfo(iPad,body)
                            if(grpcResp){
                                //console.log(grpcResp)
                                if(grpcResp.serverResp === 'OK' ||
                                    grpcResp.serverResp === 'resp DeviceState from server'){
                                    this.wsHandler.send('/'+ deviceId + '#'+ cmd + '='+body+'_success')
                                    return 'handleIncomingDone'
                                }
                            } else {
                                throw new Error('Unknown Grpc Response')
                            }                        
    
                        break;
    
                        default:
                            return undefined
                        break;
                    }
                } else {
                    throw new Error('iPad deviceId un-matched')
                }
    
            }

        } catch (error) {
            console.log(`fn: handleIncomingMessage ${error}`)
        }
    }

    public handleSystemInfo = (iPad: iPadDeviceDbInterface, cmdBody:string) => {
        return new Promise((resolve,reject) => {
            try {
                //console.log(cmdBody)
                this.systemInfo.batteryLevel = parseFloat(JSON.parse(utils.removeByteOrderMark(cmdBody)).batteryLevel)*100
                
                if(this.systemInfo.batteryLevel <= IPAD_BATTERY_LOW_LEVEL){
                    resolve(iPadSysGrpcHandler.batteryLowGrpcAdapter(iPad, this.systemInfo))
                } else {
                    //console.log('Battery Level > 25%')
                }
                
            } catch(error) {
                console.log(`fn: handleSystemInfo ${error}`)
                reject(error)
            }
        })
    }
    public handlePanicButtonRequest = (iPad: iPadDeviceDbInterface, cmdBody:string) => {
        return new Promise((resolve,reject) => {
            try{
                
                switch(cmdBody) {
                    case 'cleared':
                        resolve(iPadPanicGrpcHandler.panicButtonClearedGrpcAdapter(iPad))
                        //console.log(test)
                    break;

                    case 'pressed':
                        resolve(iPadPanicGrpcHandler.panicButtonPressedGrpcAdapter(iPad))
                        //console.log(test1)
                    break;

                    default:

                    break;
                }
            } catch(error) {
                console.log(`fn: handlePanicButtonRequest ${error}`)
                reject(error)
            }
        })
    }

    public handleLightControlRequest = (iPad: iPadDeviceDbInterface, cmdBody:string) => {
        return new Promise ((resolve,reject) => {
            try{
                console.log('handleLightControlRequest')
                console.log(cmdBody)
                //console.log(JSON.parse(cmdBody).scene.toLowerCase())

                //philioSystemHandler.doLightSceneAuto(this.philio,JSON.parse(cmdBody).scene.toLowerCase())
                philioSystemHandler.doLightSceneAuto(this.philio,cmdBody.split('=')[1].toLowerCase())

            } catch(error){
                console.log(`fn:handleLightControlRequeset ${error}`)
                reject(error)
            }
        })
    }



    
}



module.exports = iPadClientController