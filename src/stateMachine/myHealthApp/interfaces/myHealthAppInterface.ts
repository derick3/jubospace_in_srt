export interface WsConnectToken {
    "uid": string;
    "deviceId": string;
    "patientId": string;
    "roomNum"?: string;
    "bedNum"?: string;
    "isAlive"?: boolean;
    "ipAddress": string;
    "keepAliveTimeout": string;
}



