'use strict';

const deviceSystemHandler = require('./../../grpc/juboSpaceDeviceClient').grpcDeviceClientHandler
//const utils = require('./../../utils/utilities')

import { iPadDeviceDbInterface } from './../../interfaces/nanoCouchInterface'
import { GenericSystemInfo } from './../../interfaces/genericInterface'
const batteryLowGrpcAdapter = async (deviceInterface: iPadDeviceDbInterface | any,
                                    systemInfo: GenericSystemInfo) => {
    try {
        //console.log(deviceInterface)
        //console.log(systemInfo)
        return await deviceSystemHandler.DeviceStateGrpcUpdate({
            "NISPatientId": deviceInterface.patientId,
            "boxDeviceId": deviceInterface.boxDeviceId,
            "name": deviceInterface.deviceName,
            "location": deviceInterface.location.facility + ' - ' + deviceInterface.location.position,
            "room": deviceInterface.location.roomNum,
            "state": 'Battery Low',
            "description": 'Current Battery Level: ' + systemInfo.batteryLevel+'%',
            "message": '設備低電量通知: '+systemInfo.batteryLevel+'%'

        })

    } catch(error) {
        console.log(`fn: batteryLowGrpcAdapter ${error}`)
    }
}

exports.batteryLowGrpcAdapter = batteryLowGrpcAdapter


