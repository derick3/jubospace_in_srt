'use strict'

import { v4 as uuidv4 } from 'uuid';

const wss = require('ws')
const http = require('http')
const httpServer = http.createServer()
const iPadController = require('./iPadController')
const utils = require('./../../utils/utilities')
const philioController = require('./../philioZwave/systemControl/philioSystemControl')

import type { GenericSpaceInfo, GenericSystemInfo } from './../../interfaces/genericInterface'
import { WsConnectToken } from './interfaces/myHealthAppInterface'
import { iPadDeviceDbInterface } from './../../interfaces/nanoCouchInterface'
        
const WS_CONN_TIMEOUT = 30000

interface iPadControllerInterface {
    initController: () => void
    "systemInfo": GenericSystemInfo;
    handleSystemInfo: (iPad: iPadDeviceDbInterface, msg:string) => void
    handleIncomingMessage: (iPad: iPadDeviceDbInterface, msg:string) => Promise <any>
    handlePanicButtonRequest: (iPad: iPadDeviceDbInterface,cmd:string) => void
    handleLightControlRequest: (iPad: iPadDeviceDbInterface, cmd:string) => void

}

interface myHealthWsServer {
    wsServerHandler 
}

const wsConnectOptions = {
    port: 8888,
    server: httpServer,
    clientTracking: true,
}

class MyHealthAppServer implements myHealthWsServer  {
    wsServerHandler: any
    wsIpadClients: Array<WsConnectToken>
    timerHandler: any
    spaceInfo: GenericSpaceInfo
    deviceDb: any
    iPadsDeviceList: Array<iPadDeviceDbInterface>
    philioIfConfig: any

    constructor(spaceInfo : GenericSpaceInfo, deviceDbHandler: any) {
        
        this.wsIpadClients = []
        this.spaceInfo = spaceInfo
        this.deviceDb = deviceDbHandler
        this.deviceDb.getIpadDeviceList().then((resp)=>{
            this.iPadsDeviceList = resp
        })

        philioController.findPhilioGateway().then((resp)=>{
            //console.log(resp)
            this.philioIfConfig = resp
        })

        this.wsServerHandler = new wss.Server(wsConnectOptions)
        this.serverStart()
    }
    private noop = () => {}
    private interval = setInterval(function ping(){

    }, WS_CONN_TIMEOUT);

    private serverStart = () => {
        let deviceDbHandler = this.deviceDb
        try{
            this.wsServerHandler.on('connection', (ws, req) => {
                
                let iPadHandler : iPadControllerInterface
                ws.isAlive = true;
                ws.id = uuidv4()
                ws["connectAuth"] = "unknown"
                let clientIp = req.socket.remoteAddress.match(/\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/)[0]
                let clientInfo = {
                    uid: ws.id,
                    deviceId:'',
                    patientId: '',
                    roomNum:'',
                    bedNum:'',
                    ipAddress: clientIp,
                    keepAliveTimeout: ''+WS_CONN_TIMEOUT+'ms'
                }
                
                ws.send('/connectToken='+JSON.stringify(clientInfo))
                ws.on('ping',this.wsHeartbeatPing)
                ws.on('pong',this.wsHeartbeatPong)
                ws.on('error',(error)=>{
                    console.log(`error ${error}`)
                })
                ws.on('close',()=>{
                    console.log(ws.id+' => closed->terminating')
                    // Update CouchDb Device Info , ie. lastConnectTime


                    this.wsIpadClients = this.removeClient(this.wsIpadClients,'deviceId',ws.id)
                    ws.isAlive = false
                    ws.terminate()                   
                    console.log(this.wsIpadClients)
                })
                ws.on('message',(msg) => {
                    let iPadDevice:iPadDeviceDbInterface
                    try{
                        if(ws.connectAuth === undefined){
                            console.log('connecAuth undefined')
                        } else if (ws.connectAuth === 'unknown'){
                            console.log('connecAuth unknown')
                        } else if (ws.connectAuth === 'accepted'){
                            console.log('connecAuth accepted')
                        } else if (ws.connectAuth === 'rejected'){
                            console.log('connecAuth rejected')
                        } else {
                            console.log('wtf')
                        }
                        console.log(msg)
                        let head = msg.split("=")[0]
                        let body = msg.split("=")[1]

                        if(head === '/connectResp'){
                            let connectTokenResp : WsConnectToken = JSON.parse(utils.removeByteOrderMark(body))
                            //console.log(connectTokenResp)
                            iPadDevice = this.iPadsDeviceList.find((x: { deviceId: string; }) => x.deviceId === connectTokenResp.deviceId)    
                            //console.log(iPadDevice)
                            if(iPadDevice === undefined){
                                console.log('New ipad found. Creating new ipad instance in deviceDb')
                                let now = new Date(Date.now()).toISOString()
                                //console.log(connectTokenResp.deviceId)
                                iPadDevice = {
                                    deviceId: connectTokenResp.deviceId,
                                    vendorId: "",
                                    deviceName: "iPad - Jubo Space App",
                                    patientId: connectTokenResp.patientId,
                                    location: {
                                        facility: "",
                                        roomNum: connectTokenResp.roomNum,
                                        position: "",
                                        bedNum: connectTokenResp.bedNum
                                    },
                                    createdTime: now,
                                    updatedTime: "",
                                    lastBeatTime: now
                                }
                                
                                this.deviceDb.dbInsertUpdateIpad(iPadDevice).then((resp)=>{
                                    if(resp.ok === true){
                                        console.log(resp)
                                        ws.id = iPadDevice.deviceId
                                        ws.connectAuth = 'accepted'
                                        this.iPadsDeviceList.push(iPadDevice)
                                    } else {
                                        ws.connectAuth = 'rejected'
                                    }
                                })
                            } 
                                if(ws.connectAuth != undefined || ws.connectAuth === 'unknown' || ws.connectAuth != 'rejected'){
                                    //deviceId is found in couchDb
                                    ws.id = iPadDevice.deviceId
                                    ws.connectAuth = 'accepted'
                                    ws.send('/connectAuth=accepted')
                                    iPadDevice.location.roomNum = connectTokenResp.roomNum
                                    iPadDevice.location.bedNum = connectTokenResp.bedNum
                                    this.deviceDb.dbInsertUpdateIpad(iPadDevice).then((resp)=>{
                                        if(resp.ok === true){
                                            iPadHandler  = new iPadController(iPadDevice, ws, this.philioIfConfig)
                                            console.log('```````````````iPAD````````````````')
                                            console.log(iPadController)


                                            this.wsIpadClients.push({...connectTokenResp,...iPadHandler})
                                        }
                                    })
                                }

                        } else {
                            if(ws.connectAuth === 'accepted'){
                                let deviceId = head.split("#")[0].split('/')[1]
                                iPadDevice = this.iPadsDeviceList.find((x: { deviceId: string; }) => x.deviceId === deviceId)  
                                iPadHandler.handleIncomingMessage(iPadDevice, msg).then((grpcResp)=>{
                                    if(grpcResp != 'handleIncomingDone'){

                                    }
    
                                })
                            } else if(ws.connectAuth === 'rejected'){

                            } else {

                            }
                        }
                    } catch (error) {
                        console.log(`fn: ws.on('message',(msg) ${error}`)
                    }
                })
            })
            this.wsServerHandler.on('close',(a,b,c,d)=>{
                console.log(a)
                console.log(b)
                console.log(c)
                console.log(d)
                clearInterval(this.interval);
            }) 
        } catch (error) {
            console.log(`fn: serverStart ${error}`)
        } finally {
            console.log('ws server initializing')
            this.timerHandler = setInterval(this.serverHeartbeat.bind(this),WS_CONN_TIMEOUT)
        } 
    }
    private serverHeartbeat = () => {
        try {
            // console.log(this.wsIpadClients)
            // console.log(this.wsServerHandler.clients.size)
            this.wsServerHandler.clients.forEach((ws) => {
                
                if (ws.isAlive === false) return ws.terminate();
             
                ws.isAlive = false;
                ws.ping(this.noop);
              });
        } catch (error) {
            console.log(`fn: serverHeartbeat ${error}`)
            throw new Error('wss heartbroken')
        }
    }
    private wsHeartbeatPing = () => {    
        for (let client of this.wsServerHandler.clients) {
            //console.log(`ping received ${client.id}`)
            client.isAlive = true;
        }
    } 
    private wsHeartbeatPong = () => {
        for (let client of this.wsServerHandler.clients) {
            //console.log(`pong received ${client.id}`)
            client.isAlive = true;
        }
    }
    private removeClient = (array:Array<WsConnectToken>,key:string,value:string):Array<WsConnectToken> => {
        const index = array.findIndex(obj => obj[key] === value);
        return index >= 0 ? [
            ...array.slice(0, index),
            ...array.slice(index + 1)
        ] : array;
    }
    
    public updateDeviceList = () => {
        try {

        } catch(error) {
            console.log(`fn: updateDeviceList`)
        }
    }
}

module.exports = MyHealthAppServer