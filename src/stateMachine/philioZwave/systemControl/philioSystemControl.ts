'use strict'

const fs = require('fs');
const path = require('path');
const _ = require('lodash');

const Netlist = require('./../../../netlist/netlist');
const rp = require('request-promise');
const philioApiHandler = require('./../philioApi/philioApiHandler')
const philioApi = require('./../philioApi/philioDeviceApi');
const utils = require('./../../../utils/utilities')

import { 
    PhilioGatewayObject } from "./../interfaces/philioInterface";

import { 
        PhilioIfConfig,
        PhilioRespDevice, 
        PhilioRespDeviceChannel,
        PhilioEventMessage } from './../interfaces/philioZwaveInterface'

import  { 
    PhilioDevice, 
    PhilioSensor, 
    PhilioActuator,
    ActuatorFunction,
    ActuatorChannelDetail,
    SensorFunction,
    SensorChannelDetail,
    EventDbFormatOutput,
    EventSeriesInterface,
    SensorSeriesInterface,
    EventDataType,
    SensorDataType,
    nanoDbInsertResp,
    nanoDbFindResp,

                 } from './../../../interfaces/nanoCouchInterface'

const findPhilioGateway = () => {
    return new Promise ((resolve,reject) => {
        try{
            let gatewayIfName = Netlist.find("Philio");
            resolve(gatewayIfName)
        } catch(error){
            reject(error)
        }
    })
}

const initNetSetting = async (philioGateway:PhilioGatewayObject) => {
    try{        
        console.log('initNetSetting Working........')
        let connMode = await philioApiHandler.networkConfig(philioGateway, "getconnectmode", "");
        console.log("Philio  ==>  " + connMode);
        await philioApiHandler.networkConfig(philioGateway, "setconnectmode", "DHCP");
        let gatewayIp = await philioApiHandler.networkConfig(philioGateway, "getgateway", "");
        console.log("Philio  ==>  " + gatewayIp);
        let dnsServer = await philioApiHandler.networkConfig(philioGateway, "getdnsserver", "");
       // console.log("Philio  ==>  " + dnsServer);
        let netMask = await philioApiHandler.networkConfig(philioGateway, "getnetmask", "");
       // console.log("Philio  ==>  " + netMask);
        let philioIp = await philioApiHandler.networkConfig(philioGateway, "getip", "");
       // console.log("Philio  ==>  " + philioIp);

    } catch (error) {
        console.log("fn: initNetSetting: " + error);
    }
}

const eventLogHandler = async (sequence:number,ifConfig: PhilioIfConfig, philioDbHandler:any) => {
    try{
        let philioEventStr = await philioApiHandler.networkConfig(ifConfig, "jsongetevent", sequence);
        let formattedLogs:EventDbFormatOutput = await philioEventDbFormatAdapter(philioEventStr)
        
        //console.log(formattedLogs)

        if(formattedLogs.sensorLogs.length != 0){
            for(let sensorLog of formattedLogs.sensorLogs){
                //console.log(sensorLog)
                let dbFindResult:nanoDbFindResp = await philioDbHandler.dbFindSensorSeries(sensorLog)
                //console.log(dbFindResult)
                if(dbFindResult.docs.length===0){
                    philioDbHandler.dbAddNewSensorDataSeries(sensorLog).then((resp)=>{
                        console.log(`new sensor device log`)
                        console.log(resp)
                    })
                } else if(dbFindResult.docs.length===1){
                    //console.log('1')
                    let dbSensorLog = dbFindResult.docs[0].sensorLog
                    let dbSensorSeries = dbSensorLog.sensorSeries.sort((y,x)=> y.timeStamp-x.timeStamp)
                    let eventSensorSeries = sensorLog.sensorSeries.sort((y,x)=>y.timeStamp-x.timeStamp)
                    let newSensorSeries:Array<SensorDataType> = utils.filterNewDataSeries(eventSensorSeries,dbSensorSeries,['sensorVal','timeStamp'],'timeStamp')
                    if(newSensorSeries === undefined || newSensorSeries.length == 0){

                    } else {
                        philioDbHandler.dbUpdateSensorSeries(dbFindResult,newSensorSeries).then((resp)=>{
                            console.log(resp)
                        })
                    }

                } else {
                    console.log('2 conflict')
                }
            }
        } 
        
        if(formattedLogs.eventLogs.length != 0) {
            console.log('event')
            //testPhilio.dbUpdateDeviceEvent(formattedLogs.eventLogs)
            //console.log(formattedLogs.eventLogs.length)
        }
        
        if(formattedLogs.abnormalLogs.length != 0 ){
            console.log('abnormal')
            for(let abnormalLog of formattedLogs.abnormalLogs){
                let dbFindResult:nanoDbFindResp = await philioDbHandler.dbFindEventSeries(abnormalLog)
                if(dbFindResult.docs.length === 0){
                    philioDbHandler.dbAddNewAbnormalDataSeries(abnormalLog).then((resp)=>{

                    })
                } else if (dbFindResult.docs.length === 1){
                    let dbAbnormalLog = dbFindResult.docs[0].eventLog
                    let dbAbnormals = dbAbnormalLog.eventSeries.sort((y,x)=>y.timeStamp-x.timeStamp)
                    let abnormals = abnormalLog.eventSeries.sort((y,x)=>y.timeStamp-x.timeStamp)
                    let newAbnormals:Array<EventDataType> = utils.filterNewDataSeries(abnormals,dbAbnormals,['eventVal','timeStamp'],'timeStamp')
                    if(newAbnormals === undefined || newAbnormals.length == 0){

                    } else {
                        philioDbHandler.dbUpdateAbnormalDataSeries(dbFindResult,newAbnormals).then((resp)=>{
                            console.log(resp)
                        })
                    }
                } else {
                    console.log(`2++ collision`)
                }
            }
        }
        
        if(formattedLogs.motionLogs.length != 0){
            console.log(formattedLogs.motionLogs)
            for(let motionLog of formattedLogs.motionLogs){
                console.log(motionLog)
                let dbFindResult:nanoDbFindResp = await philioDbHandler.dbFindEventSeries(motionLog)
                if(dbFindResult.docs.length === 0){
                    //console.log('0')
                    philioDbHandler.dbAddNewMotionDataSeries(motionLog).then((resp)=>{
                        console.log(`new motion device log`)
                        console.log(resp)
                    })
                } else if(dbFindResult.docs.length === 1){
                    //console.log('1')
                    //console.log(dbFindResult.docs[0])
                    //console.log(motionLog)
                    let dbMotionLog = dbFindResult.docs[0].eventLog
                    let dbMotions = dbMotionLog.eventSeries.sort((y,x)=> y.timeStamp - x.timeStamp)
                    let motions = motionLog.eventSeries.sort((y,x)=> y.timeStamp - x.timeStamp)
                    let newMotions:Array<EventDataType> = utils.filterNewDataSeries(motions,dbMotions,['eventVal','timeStamp'],'timeStamp')
                    if(newMotions === undefined || newMotions.length == 0) {
                        //console.log('same in db vs events')
                        //console.log(motions)
                        //console.log(dbMotions)
                    } else {
                        //console.log('new data')
                        philioDbHandler.dbUpdateMotionDataSeries(dbFindResult,newMotions).then((resp)=>{
                            console.log(resp)
                        })
                    }

                } else {
                    console.log('2++')
                }
            }
            
        }        

        return formattedLogs;   
    } catch (error){
        console.log(`fn: eventLogHandler ${error}`)
        return error
    }
}

const readUserDefineName = async (ifConfig: PhilioIfConfig) => {
    try{
        let jsonResp = await philioApiHandler.readDeviceTargetName(ifConfig)
        //let userDefineName = await updateUserDefineNameToFile(jsonResp)
        //return userDefineName

    } catch (error) {

    }
}

const doLightSceneAuto = async (ifConfig: PhilioIfConfig, sceneName:string) => {
    const MAX_RETRIES = 10;
    for(let i=0; i<=MAX_RETRIES; i++){
        try{
            let sceneResp = await philioApiHandler.doLightScene(ifConfig,sceneName)
            console.log(sceneResp)
            if(sceneResp.split('<br>')[0]===sceneName && sceneResp.split('<br>')[1] === 'doscene done'){
                
                i=MAX_RETRIES;
                let stringResp = 'lightmode-'+sceneName
                //commonEvent.emit('done-light-scene',stringResp)
            }
        } catch (error){
            const timeout = i*1000;
            console.log('Waiting', timeout, 'msec');
            await utils.waitPromise(timeout)
            console.log("doLightSceneAuto Retrying", error.message, i);
        }
    }
}

const findCurrentSequence = (eventLogResp: string) => {
    return new Promise((resolve, reject) => {
        let tempDump;
        try{
            tempDump=eventLogResp
            if(utils.isStrJsonFormat(eventLogResp)){
                let eventlog = JSON.parse(eventLogResp).eventLog;
                let currentEpoch: number = Math.floor(Date.now()/1000);
                if(eventlog.length != 0){
                    let firstSequence = eventlog[0].sequence;
                    if(Number.isInteger(firstSequence)){
                        for (let i = 0; i < eventlog.length; i++) {
            
                            let readTimeStamp = eventlog[i].timeStamp;
                            //console.log(readTimeStamp)    
                            // Read Log within 2 minutes
                            if ((currentEpoch - readTimeStamp) < 120) {
                                let twoMinute = i+firstSequence
                                // console.log(`< Two Minute Seq: ${twoMinute}`)
                                resolve(twoMinute);
                            }
                        }
                        //console.log(`length: ${eventlog.length}`)
                        let end = eventlog[eventlog.length-1].sequence
                        //console.log(`Last Seq: ${end}`)
                        resolve(end) 
                    } else {
                        resolve(1);
                    }
                } else {
                    console.log('eventlog is empty []')
                    resolve(1)
                }

                
            } else {
                throw new Error('eventLogResp String is not valid JSON format')
            }
        } catch(error){
            console.log("fn: findCurrentSequence " + error);
            console.log(tempDump)
            reject(error)
            //JuboEvent.emit('retry-sequence');
        }
    })
}

const readDeviceTargetName = (ifConfig: PhilioIfConfig) => {
    return new Promise((resolve, reject) => {
        try{
            let deviceTargetName = {
                uid: '',
                chid: '',
                targetName: ''
            }

            if (ifConfig) {
                let connectOptions = ifConfig.connectOption
                
                connectOptions.uri = 'http://' + ifConfig.deviceIp + '/network.cgi?jsongetall'
                //console.log(connectOptions.uri)
                let respStr = rp(connectOptions)
                                    .then()
                                    .catch((error) =>{
                                        console.log(`fn:readPhilioDeviceTargetName  targetName ${error}`)
                                        //JuboEvent.emit('philioRequestError')
                                    })
                
                let respJson = JSON.parse(respStr.split('<br>')[0])
                if(respJson){
                    console.log(respJson.targetNames)


                    resolve(respJson.targetNames)
                }
            }
        } catch (error) {
            console.log(`fn: readTargetName ${error}`)
            reject(error)
        }
    })
}

const writeDeviceTargetName = (ifConfig: PhilioIfConfig) => {
    return new Promise((resolve, reject) => {
        try{
            if (ifConfig != null || ifConfig != undefined) {
                let connectOptions = ifConfig.connectOption
                
                connectOptions.uri = 'http://' + ifConfig.deviceIp + '/network.cgi?jsonsetall='
                //console.log(connectOptions.uri)
                let sceneConnect = rp(connectOptions)
                                    .then()
                                    .catch((error) =>{
                                        console.log(`fn:writePhilioDeviceTargetName  sceneConnect ${error}`)
                                        //JuboEvent.emit('philioRequestError')
                                    })
            
                resolve(sceneConnect);
            } else {

            }
        } catch (error){
            console.log(`fn: writeUserDefineName ${error}`)
            reject(error)
        }
    })
}

const procJsonGetAll = async (ifConfig: PhilioIfConfig) => {
    try {
            let queryResult = await philioApiHandler.jsonGetAll(ifConfig)
            let queryStr = queryResult.split('<br>')[0]
            if(utils.isStrJsonFormat(queryStr)){
                let jsonGetAllObj = JSON.parse(queryStr)
                return jsonGetAllObj
            }
            
    } catch(error) {
        console.log(`fn: procJsonGetAll ${error}`)
        
    }
}

const philioDeviceDbFormatAdapter = (philioDevices:Array<PhilioRespDevice>):Promise<PhilioDevice[]> => {
    return new Promise((resolve,reject)=>{
        let queryArray: Array<PhilioDevice|PhilioActuator|PhilioSensor> = []
        try{
            for(let {
                "home_id": homeId,
                "uid": uid,
                "Product ID": productId,
                "battery": battery,
                "channel": deviceChannel

            } of philioDevices){
                //console.log(deviceChannel)
                let queryTemp: PhilioDevice|PhilioActuator|PhilioSensor = {
                    "deviceId": '',
                    "vendorId": '',
                    "deviceName": '',
                    "description": '',
                    "deviceType": '',
                    "location": {
                        "facility": '',
                        "roomNum": '',
                        "position": ''
                    },
                    "createdTime": '',
                    "updatedTime": '',
                    "lastBeatTime": '',
                    "currentStatus": '' ,
                    "powerSource": '',
                    "currentPowerLevel": '',
                    "functions": []

                }
                let channels:Array<PhilioRespDeviceChannel> = deviceChannel
                queryTemp.vendorId = homeId+'/'+uid
                queryTemp.deviceName = philioApi.productInfo.find((x:{productId:string})=> x.productId === productId.toString()).part
                queryTemp.description = philioApi.productInfo.find((x:{productId:string})=> x.productId === productId.toString()).product
                queryTemp.createdTime = new Date(Date.now()).toISOString()
                queryTemp.updatedTime = new Date(Date.now()).toISOString()
                queryTemp.lastBeatTime = new Date(Date.now()).toISOString()
                queryTemp.powerSource = (battery === 255 )? 'AC':'Battery'
                queryTemp.currentPowerLevel = (battery === 255)? 'n/a': battery.toString()
                //console.log(queryTemp)

                for(let channel of channels){
                    let channelFunction = <ActuatorFunction> {}
                    let sensorFunction = <SensorFunction> {}
                    if(channel.name.includes('Switch') || channel.name.includes('switch')){
                        queryTemp.deviceType = 'actuator'
                        if(channel.chid != 0){
                            let channelDetail:ActuatorChannelDetail = {
                                "chid": channel.chid.toString(),
                                "currentState": (channel.basicvalue === 255)? 'ON':'OFF'
                            }
                            
                            channelFunction.channels = channelDetail
                            channelFunction.type = 'actuator'
                            channelFunction.on = '255'
                            channelFunction.off = '0'
                            
                            //console.log(channelFunction)
                            
                            queryTemp.functions.push(channelFunction)
                        }

                    } else if (channel.name.includes('Dim') || channel.name.includes('dim')) {
                        //console.log(channel)
                        queryTemp.deviceType = 'dimmer'
                        
                            let channelDetail:ActuatorChannelDetail = {
                                "chid": channel.chid.toString(),
                                "currentState": (channel.basicvalue >= 0)? 'ON':'OFF'
                            }
                            
                            channelFunction.channels = channelDetail
                            channelFunction.type = 'dimmer'
                            channelFunction.min = '0'
                            channelFunction.max = '100'
                            
                            //console.log(channelFunction)

                            queryTemp.functions.push(channelFunction)
                                          
                        
                    } else if (channel.name.includes('Sens') || 
                                channel.name.includes('sens') ||
                                channel.name.includes('PIR') ||
                                channel.name.includes('pir')) {    //sensor here
                        //console.log(channel)
                        queryTemp.deviceType = 'sensor'
                        if(channel.chid != 0){

                            let channelDetail:SensorChannelDetail = {
                                "chid": channel.chid.toString(),
                                "name": channel.name,
                                "valueScale": philioApi.sensorUnit.find((x:{sensorUnit:string})=>x.sensorUnit === channel.sensorunit.toString()).scale,
                                "sensorUnit": philioApi.sensorUnit.find((x:{sensorUnit:string})=>x.sensorUnit === channel.sensorunit.toString()).unitSymbol,
                                "lastValue": channel.sensorvalue.toString()
                            }
                            sensorFunction.channels = channelDetail
                            queryTemp.functions.push(sensorFunction)
                        }
                    } else if (channel.name.includes('UNKNO')){
                        //console.log(channel)
                    } else {
                        console.log(channel)
                        throw new Error('getPhilioDevicesToDbAdapter unhandled channel '+ channel)
                    }    
                }
                queryArray.push(queryTemp)   
            }
            //console.log(queryArray)
            resolve(queryArray)   

        } catch(error){
            reject(error)
        }
    })
}

//Philio eventLog eventCode
const TAMPER_TRIGGER = 4001
const LOW_BATTERY = 4002
const BATTERY_CHANGE = 4003

const PIR_TRIGGER = 4101
const DOOR_OPEN = 4102
const DOOR_CLOSE = 4103
const SMOKE_TRIGGER = 4104
const CO_TRIGGER = 4105
const CO2_TRIGGER = 4106
const FLOOD_TRIGGER = 4107
const GLASS_BREAK = 4108
const GPIO_ON = 4109
const GPIO_OFF = 4110

const TEMPERATURE_OVER = 4111
const TEMPERATURE_UNDER = 4112
const ILLUMINATION_OVER = 4113
const ILLUMINATION_UNDER = 4114
const HUMIDITY_OVER = 4115
const HUMIDITY_UNDER = 4116


const TEMPERATURE_REPORT = 4801
const ILLUMINATION_REPORT = 4802
const HUMIDITY_REPORT = 4803
const METER_REPORT = 4804
const CO2_REPORT = 4805
const VOC_REPORT = 4806
const UV_REPORT = 4807

const STATUS_UPDATE = 5002
const CONFIG_CHANGE = 5003

const philioEventDbFormatAdapter = (eventLog:string):Promise<EventDbFormatOutput> => {
    return new Promise((resolve,reject)=>{
        try{
            eventLog = eventLog.split('<br>')[0]
            let eventLogs = <EventDbFormatOutput> {
                "sensorLogs": [],
                "eventLogs": [],
                "abnormalLogs": [],
                "motionLogs": []
            }
            if(utils.isStrJsonFormat(eventLog)){
                let events:Array<PhilioEventMessage>= JSON.parse(eventLog).eventLog
                for(let event of events){
                    if(event.eventCode >= TEMPERATURE_REPORT && event.eventCode <= HUMIDITY_REPORT){
                        //SENSORLOG
                        if(event.funcName.includes('UNKNOW') === false){
                            let vendorId = event.home_id+'/'+event.uid
                            let log = eventLogs.sensorLogs.find((x:{vendorId:string,sensorType:string})=>(x.vendorId===vendorId &&x.sensorType===event.funcName))
                            let sensorType = philioApi.eventCode.find((x:{eventCode:number})=>(x.eventCode === event.eventCode)).sensorType
                            //let sensorSearch = eventLogs.sensorLogs.find((x:{sensorType:string})=>x.sensorType===event.funcName)
                            if( log === undefined ){
                                let sensorlog : SensorSeriesInterface = {
                                    "sensorType": sensorType,
                                    "vendorId": event.home_id+'/'+event.uid,
                                    "channelId": event.channelID.toString(),
                                    "updatedTimeStamp": new Date(Date.now()).toISOString(),
                                    "lastSensorValue": event.sensorValue,
                                    "dataUnit": philioApi.sensorUnit.find((x:{sensorUnit:string})=>x.sensorUnit === event.dataUnit.toString()).unitSymbol,
                                    "valueScale": philioApi.sensorUnit.find((x:{sensorUnit:string})=>x.sensorUnit === event.dataUnit.toString()).scale,
                                    "sensorSeries": [{
                                        "sensorVal": event.sensorValue,
                                        "timeStamp": event.timeStamp * 1000 + event.timeStamp_ms
                                    }]
        
                                }
                            //console.log(sensorlog)
                            eventLogs.sensorLogs.push(sensorlog)                            
                            } else {
                                log.sensorSeries.push({
                                    "sensorVal": event.sensorValue,
                                    "timeStamp": event.timeStamp * 1000 + event.timeStamp_ms
                                })
                            }
                            //console.log(eventLogs.sensorLogs)                            
                        }

                    }

                    if (event.eventCode >= PIR_TRIGGER && event.eventCode <= DOOR_CLOSE){
                        //EVENTLOG
                        //console.log(event)
                        let eventType = ''
                        let eventMessage = ''
                        let vendorId = event.home_id+'/'+event.uid
                        switch(event.eventCode){
                            case PIR_TRIGGER: 
                                eventType='pir'; 
                                eventMessage='pirTrigger';
                                break;
                            case DOOR_OPEN: 
                                eventMessage = 'openDoor'
                                eventType = 'door'
                                break;
                            case DOOR_CLOSE: 
                                eventType='door'
                                eventMessage = 'closeDoor'
                                break;
                            default: eventType=''; break;    
                        }
                        
                        let log =eventLogs.motionLogs.find((x:{vendorId:string})=>x.vendorId===vendorId)

                        if(log === undefined){
                            let doorMotionLog : EventSeriesInterface = {
                                "eventType": eventType,
                                "vendorId": vendorId,
                                "channelId": event.channelID.toString(),
                                "lastEventVal": eventMessage,
                                "updatedTimeStamp": new Date(Date.now()).toISOString(),
                                "eventSeries": [{
                                    "eventVal": eventMessage,
                                    "timeStamp": event.timeStamp * 1000 + event.timeStamp_ms
                                }]
                            }
                            eventLogs.motionLogs.push(doorMotionLog)
                        } else {
                            //console.log('repeated')
                            log.eventSeries.push({
                                "eventVal": eventMessage,
                                "timeStamp": event.timeStamp * 1000 + event.timeStamp_ms
                            })
                        }
                        
                        //console.log(eventLogs.motionLogs)
                    } else if (event.eventCode >= TAMPER_TRIGGER && event.eventCode <= BATTERY_CHANGE){
                        let eventMessage = ''
                        let eventType = ''
                        let vendorId = event.home_id+'/'+event.uid
                        switch(event.eventCode){
                            case TAMPER_TRIGGER: 
                                eventMessage='Device Tampered'; 
                                eventType='tamper_detect';
                                break;
                            case LOW_BATTERY: 
                                eventMessage='Device Low Battery'; 
                                eventType='low_battery';
                                break;
                            case BATTERY_CHANGE: 
                                eventMessage='Device Battery Changed'; 
                                eventType='battery_change'
                                break;
                            default: eventMessage=''; break;    
                        }

                        let log =eventLogs.abnormalLogs.find((x:{vendorId:string})=>x.vendorId===vendorId)
                        if(log === undefined){
                            // new device abnormal log series
                            let abnormalLog : EventSeriesInterface = {
                                "eventType": eventType,
                                "vendorId": event.home_id+'/'+event.uid,
                                "channelId": event.channelID.toString(),
                                "lastEventVal": eventMessage,
                                "updatedTimeStamp": new Date(Date.now()).toISOString(),
                                "eventSeries":[{
                                    "eventVal": eventMessage,
                                    "timeStamp": event.timeStamp * 1000 + event.timeStamp_ms
                                }]
    
                            }
                            //console.log(abnormalLog)
                            eventLogs.abnormalLogs.push(abnormalLog)
                        } else {
                            //update existing
                            log.eventSeries.push({
                                "eventVal": eventMessage,
                                "timeStamp": event.timeStamp * 1000 + event.timeStamp_ms
                            })
                        }


                    } else if(event.eventCode >= SMOKE_TRIGGER && event.eventCode <= GPIO_OFF ||
                                event.eventCode >= TEMPERATURE_OVER && event.eventCode <= HUMIDITY_UNDER) {

                        let vendorId = event.home_id+'/'+event.uid   
                        
                        let log =eventLogs.eventLogs.find((x:{vendorId:string})=>x.vendorId===vendorId)
                        if(log === undefined){
                            let eventLog : EventSeriesInterface = {
                                "eventType": event.funcName,
                                "vendorId": event.home_id+'/'+event.uid,
                                "channelId": event.channelID.toString(),
                                "updatedTimeStamp": new Date(Date.now()).toISOString(),
                                "eventSeries": [{
                                    "eventVal": event.eventCode,
                                    "timeStamp": event.timeStamp * 1000 + event.timeStamp_ms
                                }]
                            }
                            //console.log(eventLog)
                            eventLogs.eventLogs.push(eventLog)
                        } else {
                            log.eventSeries.push({
                                "eventVal": event.eventCode,
                                "timeStamp": event.timeStamp * 1000 + event.timeStamp_ms                                
                            })
                        }
                    }
                }
                resolve(eventLogs)
            } else {
                throw new Error('fn: philioEventDbFormatAdapter receive invalid JSON string: '+eventLog)
            }
        } catch(error){
            console.log(`fn: philioEventDbFormatAdapter ${error}`)
            reject(error)
        }
    })
}

exports.philioDeviceDbFormatAdapter = philioDeviceDbFormatAdapter;
exports.philioEventDbFormatAdapter = philioEventDbFormatAdapter;

exports.findPhilioGateway = findPhilioGateway;
exports.initNetSetting = initNetSetting;

exports.eventLogHandler = eventLogHandler;


exports.writeDeviceTargetName = writeDeviceTargetName
exports.readDeviceTargetName = readDeviceTargetName

exports.doLightSceneAuto = doLightSceneAuto;

exports.findCurrentSequence = findCurrentSequence;

exports.procJsonGetAll = procJsonGetAll

