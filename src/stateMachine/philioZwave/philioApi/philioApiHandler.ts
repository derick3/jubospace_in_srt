"use strict";
const fs = require('fs');
const sleep = require('util').promisify(setTimeout)
const JuboEvent = require('./../../../commonEvent/commonEvent').commonEmitter
const Netlist = require('./../../../netlist/netlist');

const rp = require('request-promise');
const philioApi = require('./philioDeviceApi');
const utils = require('./../../../utils/utilities')

import { 
            PhilioCmdObject,
            PhilioOptionalCmd,
            PhilioGatewayObject } from "./../interfaces/philioInterface";

import { PhilioIfConfig,PhilioRespDevice } from './../interfaces/philioZwaveInterface'
          
const networkConfig = (ifConfig: PhilioIfConfig, queryCmd: string, val: number | string | undefined) => {
    return new Promise((resolve, reject) => {
        try{
            // console.log(deviceObject);
            if (ifConfig) {
                let connectOptions = ifConfig.connectOption
                let queryResult = "";
                if (queryCmd == "jsongetevent") {
                    
                    if (val === undefined) {
                        queryResult = philioApi.queryCmd.find((x: { cmd: any; }) => x.cmd === queryCmd).query;
                    } else if (typeof val === "string" || typeof val === "number") {
                        queryResult = philioApi.queryCmd.find((x: { cmd: any; }) => x.cmd === queryCmd).query + "=" + val;
                    }
                    //console.log(queryResult)
                } else if (queryCmd == 'reboot'){
                    console.log('~~~~~~~~Philio reboot comand ~~~~~')
                    queryResult = philioApi.queryCmd.find((x: { cmd: any; }) => x.cmd === queryCmd).query + val;
                    console.log(queryResult)
                } else {
                    queryResult = philioApi.queryCmd.find((x: { cmd: any; }) => x.cmd === queryCmd).query + val;
                }
    
                connectOptions.uri = 'http://' + ifConfig.deviceIp + queryResult;

                rp(connectOptions).then((resp:string)=>{
                    let respStr = resp.split('<br>')[0]
                    //console.log(respStr)
                    resolve(respStr)
                })
                
            }
        } catch (error) {
            console.log(`fn: PhilioNetworkConfig  netConfigOption ${error}`)
            if(error.name==="RequestError"){
                //JuboEvent.emit('philioRequestError')
            }
            reject(error)
        }
    });
}   // end PhilioNetworkConfig

const controlConfig = (ifConfig: PhilioIfConfig,
    ctrlObj: PhilioCmdObject,
    optional: PhilioOptionalCmd) => {
    return new Promise((resolve, reject) => {

        let jsonCmd: string = '';
        let connectOptions = ifConfig.connectOption
        //console.log(ctrlObj)
        //console.log(optional)
        try {
            switch (ctrlObj.cmd) {

                case 'getconfig':
                    jsonCmd = '{"control":' + JSON.stringify(philioApi.ctrlCmd.find((x: { cmd: string; }) => x.cmd === ctrlObj.cmd)) + '}';
                    //console.log(jsonCmd);
                    connectOptions.uri = 'http://' + ifConfig.deviceIp + '/sdk.cgi?json=' + jsonCmd;
                    break;

                case 'switch':
                    jsonCmd = '{"control":' +
                        JSON.stringify({ ...ctrlObj, ...optional }) +
                        '}';
                    connectOptions.uri = 'http://' + ifConfig.deviceIp + '/sdk.cgi?json=' + jsonCmd;
                    //console.log(connectOptions.uri)
                    break;

                case 'sysrtctime':
                    jsonCmd = '{"control":' + JSON.stringify(philioApi.ctrlCmd.find((x: { cmd: string; }) => x.cmd === ctrlObj.cmd)) + '}';
                    connectOptions.uri = 'http://' + ifConfig.deviceIp + '/sdk.cgi?json=';//+source;
                    break;

                default:
                    //throw new PhiloError("Undefined command.");
                    break;
            }
            rp(connectOptions).then((resp:string)=>{
                resolve(resp.split('<br>')[0])
            })
            
        } catch (error) {
            console.log(`fn:ControlPhilio  controlPhilioOption ${error}`)
            if(error.name==="RequestError"){
                JuboEvent.emit('philioRequestError')
            }   
        }
    });
}   //end controlConfig

const getDevices = (ifConfig: PhilioIfConfig, deviceUid ?:string): Promise<PhilioRespDevice> => {
    return new Promise((resolve, reject) => {
        let connectOption = ifConfig.connectOption
        try {
            if(deviceUid === null || deviceUid === undefined){
                connectOption.uri = 'http://' + ifConfig.deviceIp + '/sdk.cgi?json={"control":{"cmd":"getdevice"}}';
            } else {
                connectOption.uri = 'http://' + ifConfig.deviceIp + '/sdk.cgi?json={"control":{"cmd":"getdevice","uid":'+ deviceUid +'}}';
            }
            rp(connectOption).then((resp: string)=>{
                //console.log(resp)
                if(utils.isStrJsonFormat(resp)){
                    let respJson = JSON.parse(resp)
                    if(respJson.control.respmsg === 'OK'){
                        resolve(respJson.device)
                    } else {
                        console.log(respJson.control)
                    }
                } else {
                    console.log(resp)
                    reject(new Error('getdevice respond with non-json format'))
                }

            })

        } catch (error) {
            console.log(`fn: getDevices ${error}`)
            reject(error)
        }
    });
}   //end getDevices

const getInterface = (ifConfig: PhilioIfConfig) => {
    return new Promise((resolve, reject) => {
        let connectOptions = ifConfig.connectOption
        try {
            connectOptions.uri = 'http://' + ifConfig.deviceIp + '/sdk.cgi?json={"control":{"cmd":"getinterface","uid":"0"}}';
            let philioConnection = rp(connectOptions)
            //console.log(philioConnection)
            resolve(philioConnection);
        } catch (error) {
            console.log(`fn: PhilioGetInterface  philioConnection ${error}`)
            if(error.name==="RequestError"){
                JuboEvent.emit('philioRequestError')
            }   
        }
    });
}   //end PhilioGetDevices


const doLightScene = (ifConfig: PhilioIfConfig , Scene:string) => {
    return new Promise((resolve, reject) => {
        try{
            console.log(ifConfig.connectOption)
            if (ifConfig) {
                let connectOptions = ifConfig.connectOption
                //let sceneSelect = ""
                connectOptions.uri = 'http://' + ifConfig.deviceIp + '/network.cgi?doscene=' + Scene;
                console.log(connectOptions.uri)
                let sceneConnect = rp(connectOptions)
                                    .then()
                                    .catch((error) =>{
                                        console.log(`fn:doLightScene  sceneConnect ${error}`)
                                        JuboEvent.emit('philioRequestError')
                                    })
            
                resolve(sceneConnect);
            }
        } catch (error) {
            console.log(`fn: doLightScene ${error}`)
            reject(error)
        }
    })
}

const findPhilioGateway = () => {
    return new Promise ((resolve,reject) => {
        try{
            let Gateway = Netlist.find("Philio");
            resolve(Gateway)
        } catch(error){
            reject(error)
        }
    })
}

const jsonDelete = (ifConfig: PhilioIfConfig,
                        jsonCommandStr : string ) => {
    return new Promise ((resolve, reject) => {
        try{
            if(ifConfig){
                
                let connectOption = ifConfig.connectOption
                connectOption.uri = 'http://' + ifConfig.deviceIp + '/network.cgi?jsondel=' + jsonCommandStr
                let resp = rp(connectOption)
                            .catch((error) => {
                                console.log(`fn:jsonDelete ${error}`)
                                throw new Error('philioRequestError')                                 
                            })
                resolve(resp)
            }
        } catch (error) {
            console.log(`fn: jsonDelete ${error}`)
            reject(error)
        }
    })
}

const jsonSetAll = (ifConfig: PhilioIfConfig,
                        jsonCommandStr: string ) => {
    return new Promise ((resolve,reject) => {
        try {
            if(ifConfig){
                
                let connectOption = ifConfig.connectOption
                connectOption.uri = 'http://' + ifConfig.deviceIp + '/network.cgi?jsonsetall=' + jsonCommandStr
                console.log(connectOption)
                let resp = rp(connectOption)
                            .catch((error)=>{
                                console.log(`fn:jsonSetAll ${error}`)
                                throw new Error('philioRequestError')  
                            })
                resolve(resp)
            }
        } catch (error) {
            console.log(`fn: jsonSetAll ${error}`)
            reject(error)
        }
    })
}

const jsonGetAll = (ifConfig: PhilioIfConfig) => {
    return new Promise ((resolve,reject) =>{
        try{
            if(ifConfig){
                let connectOption = ifConfig.connectOption
                connectOption.uri = 'http://' + ifConfig.deviceIp + '/network.cgi?jsongetall'

                let resp = rp(connectOption)
                            .catch((error)=>{
                                console.log(`fn:philioJsonGetAll  respStr ${error}`)
                                throw new Error('philioRequestError')
                            })
                resolve(resp)
            }             
        } catch(error){
            console.log(`fn: jsonGetAll ${error}`)
            reject(error)
        }
    })
}

exports.networkConfig = networkConfig;
exports.controlConfig = controlConfig;

exports.jsonDelete = jsonDelete
exports.jsonGetAll = jsonGetAll
exports.jsonSetAll = jsonSetAll

exports.getInterface = getInterface;
exports.getDevices = getDevices;

exports.doLightScene = doLightScene;

exports.findPhilioGateway = findPhilioGateway;
exports.philioApi=philioApi;

