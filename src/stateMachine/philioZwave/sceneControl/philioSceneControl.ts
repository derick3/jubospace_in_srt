'use strict'

const philioApi = require('./../philioApi/philioApiHandler')
const rp = require('request-promise');

import { PhilioGatewayObject,
        PhilioChannel } from './../interfaces/philioInterface';

interface PhilioScene {

}

const addScene = async (deviceObject: PhilioGatewayObject,
    sceneList: any,
    sceneTitle: string,
    sceneIcon?: string,
    componentObject?: any,
    componentValue?: number,) => {
    try{
        if(deviceObject && sceneList){
        
            if(componentObject === undefined){
                //if sceneList ! contain sceneTitle
                let jsonStr = '{\"scenes\":['
                            + '{\"title\":\"' + sceneTitle + '\"'
                            + ( typeof sceneIcon !== undefined ? ',\"icon_name":\"' + sceneIcon + '\"': '')
                            + '}]}'

                let result = await philioApi.jsonSetAll(deviceObject,jsonStr)
                
                if(result === 'jsonsetall done'){
                    return true
                } else if(result === 'jsonsetall failed') {
                    throw new Error('Fail addScene')
                }                
            } else {
                let jsonStr = '{\"scenes\":['
                            + '{\"title\":\"' + sceneTitle + '\"'
                            + ( typeof sceneIcon !== undefined ? ',\"icon_name":\"' + sceneIcon + '\"': '')
                            + '},\"comps\":[{"target\":{\"ch\":'
                            + componentObject.chid + ',\"dev\":'
                            + componentObject.uid + ',\"if\":256}'
                            + ( typeof componentValue !== undefined ? ',\"value\":' + componentValue : '')
                            + '}]}]}'
                console.log(jsonStr)
                let result = await philioApi.jsonSetAll(deviceObject,jsonStr)
                if(result === 'jsonsetall done'){
                    return true
                } else if(result === 'jsonsetall failed') {
                    throw new Error('Fail addScene')
                } 
            }


        } else {
            throw new Error('Invalid Gateway or Scene Settings')
        }
    } catch (error) {
        console.log(`fn: addScene ${error}`)
    }
}

const delScene = async (deviceObject: PhilioGatewayObject,
    sceneList: any,
    sceneTitle: string,
    componentObject?: any,
    componentValue?: number,
    sceneIcon?: string) => {
    try{
        if(deviceObject){
            let jsonCommand ={
                "scenes": [
                    {
                        "title": sceneTitle
                    }]
            }
            let result = await philioApi.jsonDelete(deviceObject,JSON.stringify(jsonCommand))
            console.log(result)
            if(result === 'jsonsetall done'){
                return true
            } else if(result === 'jsonsetall failed') {
                throw new Error('Fail addScene')
            }
        }
    } catch (error) {
        console.log(`fn: addScene ${error}`)
    }
}

const modifyScene = async (deviceObject: PhilioGatewayObject,
    sceneList: any,
    sceneTitle: string,
    componentObject?: any,
    componentValue?: number,
    sceneIcon?: string) => {
    try{
        if(deviceObject){
            let jsonCommand ={
                "scenes": [
                    {
                        "title": sceneTitle,
                        "icon_name": sceneIcon
                    }]
            }
            let result = await philioApi.jsonSetAll(deviceObject,JSON.stringify(jsonCommand))
            console.log(result)
            if(result === 'jsonsetall done'){
                return true
            } else if(result === 'jsonsetall failed') {
                throw new Error('Fail addScene')
            }
        }
    } catch (error) {
        console.log(`fn: modifySceneName ${error}`)
    }
}

const addComponentInScene = async (deviceObject : PhilioGatewayObject,
                            sceneTitle : string,
                            componentObject : any) => {
    try{
        if(deviceObject){
            let jsonCommand ={
                "scenes": [
                    {
                        "title": sceneTitle,
                        "comp": []
                    }]
            }
            let components 
            let result = await philioApi.jsonSetAll(deviceObject,JSON.stringify(jsonCommand))
            console.log(result)
            if(result === 'jsonsetall done'){
                return true
            } else if(result === 'jsonsetall failed') {
                throw new Error('Fail addScene')
            }
        }
    } catch (error) {
        console.log(`fn: modifySceneName ${error}`)
    }
}

const delComponentInScene = (deviceObject : PhilioGatewayObject,
                            sceneTitle : string, 
                            componentObject : PhilioChannel) => {


}


exports.addScene = addScene
exports.delScene = delScene
exports.modifySceneIcon = modifyScene
exports.addComponentInScene = addComponentInScene
exports.delComponentInScene = delComponentInScene
