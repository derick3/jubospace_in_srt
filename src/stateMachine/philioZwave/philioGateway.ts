'use strict';

const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const fsm = require('./../statemachine')
const utils = require('./../../utils/utilities')
const systemDefinition = require('./systemControl/philioSystemSchema')
const philioSystem = require('./systemControl/philioSystemControl')
const sceneController = require('./sceneControl/philioSceneControl')
const apiHandler = require('./philioApi/philioApiHandler')
const philioDeviceDb = require('./../../nanoCouchDb/juboPhilioDeviceDb')

const TIME_METHOD_INTERVAL = 6000
const PHILIO_SEQUENCE_RESET = 10

import { EventEmitter } from "events";

import { GenericNetworkDevice, GenericSpaceInfo } from './../../interfaces/genericInterface'
import { JuboBoxInfo } from './../../interfaces/nanoCouchInterface'
import { nanoDbFindResp } from './../../interfaces/nanoCouchInterface'

type GenericConnectAuth = {
    "user": string;
    "pass": string;
    "sendImmediately": boolean;
}
class PhilioGateway extends EventEmitter {
    
    fsMachine : any
    gatewayState : string
    deviceEventSequence : number
    timerCounter : number

    philioIfConfig : any
    timerHandler : any
    philioDbHandler : any
    philioReady : boolean

    serverIfConfig : GenericNetworkDevice
    deviceList: any
    sceneList: any
    macroList: any
    deviceNickname: any
    deviceChannel: any
    roomNum: number
    connectOption: any

    deviceDb: any
    spaceInfo: GenericSpaceInfo
    juboBoxInfo: JuboBoxInfo

    constructor (spaceInfo : GenericSpaceInfo){
        super()
        this.deviceEventSequence = 1;
        this.timerCounter = 0;
        this.connectOption = {
            uri: '',
            port: 80,
            method: 'GET',
            auth: {
                'user': 'admin', 
                'pass': '888888',
                'sendImmediately': true
            }
        }
        this.spaceInfo = spaceInfo
        
        this.philioDbHandler = new philioDeviceDb()
        this.philioReady = false
        //this.roomNum = parseInt(this.juboBoxInfo.location.roomNum)
        this.serverIfConfig = spaceInfo.netInfo
        this.fsMachine = fsm.createStateMachine(systemDefinition)
        this.gatewayState = this.fsMachine.value

        this.philioInit().then(()=>{
            
        })
    }
    private philioInit = async () => {
        try{
            console.log('----entering philioInit-----')
            this.philioIfConfig = await philioSystem.findPhilioGateway()
            console.log('------ifconfig---------')
            console.log(this.philioIfConfig)
            let ifaceResp = await apiHandler.getInterface(this.philioIfConfig)
            //this.philioGateway = await philioSystem.createPhilioGateway(this.philioIfConfig,this.connectOption,ifaceResp)
            //console.log(this.philioGateway)
            if(this.philioIfConfig){
                console.log('----------- Philio Initializing -----------') 
                await philioSystem.initNetSetting(this.philioIfConfig, this.serverIfConfig)
                let philioJsonResp = await apiHandler.getDevices(this.philioIfConfig)
                // console.log(philioJsonResp)

                let philioDevices = await philioSystem.philioDeviceDbFormatAdapter(philioJsonResp)

                for(let device of philioDevices){
                    let dbResult:nanoDbFindResp = await this.philioDbHandler.dbFindDeviceByVendorId(device)
                    if(dbResult.docs.length === 0){
                        console.log('0')
                        console.log(device)
                        this.philioDbHandler.dbAddNewPhilioDevice(device).then((resp)=>{
    
                        })
                    } else if (dbResult.docs.length === 1 ){
                        //console.log('1')
                        this.philioDbHandler.dbUpdatePhilioDevice(dbResult, device)
                    } else {
                        console.log('2++ data collision')
                    }
                }

                philioSystem.procJsonGetAll(this.philioIfConfig)
                                .then((result)=>{
                                    this.sceneList = result.scenes
                                    this.macroList = result.macros
                                    this.deviceNickname = result.targetNames
                                })                          
                this.gatewayState = this.fsMachine.transitions(this.gatewayState,'philioSequence')    
            }
        } catch (error){
            console.log(error);
            //SystemEvent.emit('retry-sequence')
        } finally {
            console.log('done-philioInit-routine')
            this.philioReady = true
            this.timerHandler = setInterval(this.timedMethods.bind(this),TIME_METHOD_INTERVAL)
        }
    }

    private timedMethods = () => {
        try{
            let now = new Date().toISOString()
            console.log(`[state]: ${this.gatewayState} [seq]: ${ this.deviceEventSequence}: ${now}`)
            switch(this.gatewayState){
                case 'philioInit':

                break;

                case 'philioSequence':
                    this.getCurrentSequence ()
                break;

                case 'philioNormal':
                    //console.log(this.timerCounter)
                    this.normalPhilioRoutine ()
                    if(this.timerCounter >= PHILIO_SEQUENCE_RESET){
                        this.gatewayState = this.fsMachine.transitions(this.gatewayState,'philioSequence')
                        this.deviceEventSequence = 1
                        this.timerCounter = 0;
                    }                    
                    //this.testing()
                break;

                case 'philioReboot':

                break;

                default:
                    this.gatewayState = 'philioInit'
                break;
            }

            this.timerCounter++;
        } catch (error) {

        }
    }

    private getCurrentSequence = async () => {
        const MAX_RETRIES = 10;
        for(let i=0; i<=MAX_RETRIES; i++){
            try{
                let eventLogListInital 
                
                eventLogListInital = await apiHandler.networkConfig(this.philioIfConfig, "jsongetevent", this.deviceEventSequence);
                this.deviceEventSequence = await philioSystem.findCurrentSequence(eventLogListInital);
                if(this.deviceEventSequence != null || this.deviceEventSequence != undefined){
                    i=MAX_RETRIES
                } else {
                    throw new Error('Invalid Log Sequence')
                }
                
            } catch (error){
                const timeout = i*1000;
                console.log('Waiting', timeout, 'msec');
                await utils.waitPromise(timeout)
                console.log("getCurrentSequence Retrying", error.message, i);
    
            } finally {
                if(this.deviceEventSequence>=1){
                    this.gatewayState = this.fsMachine.transitions(this.gatewayState,'philioNormal')
                   //console.log(`getCurrentSequence ${this.deviceEventSequence}`)
                }
                break;
            }
        }
        //console.log("start seq: "+this.deviceEventSequence)  
    }

    private testing = () => {
        console.log('testing codes')
        let test = apiHandler.jsonGetAll(this.philioIfConfig)
        sceneController.addScene(this.philioIfConfig,this.sceneList,'loveyouall','omg')
        sceneController.addScene(this.philioIfConfig,this.sceneList,'lovemetoo','omw',
            {
                "uid": 271,
                "homeId": "E8F8F146",
                "productId": "PAN04-A",
                "functype": "Meter Switch",
                "chid": 1,
                "name": "Wall Switch  CH1",
                "group": "",
                "location": "",
                "description": "In-Wall Switch (500 Series)",
                "userDefineName": "",
                "sceneValMax": 255,
                "sceneValMin": 0
            },
            255
        )
        //sceneController.delScene(this.philioIfConfig,'loveyouall')
        sceneController.modifySceneIcon(this.philioIfConfig,'loveyouall','beeboo')
        console.log(test)
    }

    private normalPhilioRoutine = () => {
        philioSystem.eventLogHandler(this.deviceEventSequence,this.philioIfConfig, this.philioDbHandler)
        .catch((error)=>{
            console.log(`fn: normalPhilioRoutine : ${error}`);
            this.deviceEventSequence=1;
            this.philioReady=false;
            this.gatewayState = this.fsMachine.transitions(this.gatewayState,'philioSequence')
        }).then((result)=>{

            //console.log(result)
            //console.log(result)
        })
    }
}

module.exports = PhilioGateway;