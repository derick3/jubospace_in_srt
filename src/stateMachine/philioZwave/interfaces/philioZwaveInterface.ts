export interface PhilioIfConfig {
    "deviceVendor": string;
    "deviceIp": string;
    "deviceMac": string;
    "deviceHostname": string | undefined | null;
    "isDeviceAlive": boolean;
    "connectOption": PhilioConnectOption
}

export interface PhilioConnectOption {
    "uri": string;
    "port": number;
    "method": string;
    "auth": PhilioAuthOption;
}


export interface PhilioAuthOption {
    user: string;
    pass: string;
    sendImmediately: boolean;
}

export interface PhilioGetEventResp {
    "Now_timeStamp": number;
    "checkbox": number;
    "deoscene_n_run": number;
    "gateway_app_star": number;
    "scene_device_number_total": number;
    "scene_device_success_number": number;
    "scene_exec_check_enable": number;
    "scene_success": number;
    "send.dat_ts": number;
    "eventLog": Array<PhilioEventMessage>
}

export interface PhilioEventMessage {
    "Scene_schedule_time": number;
    "basicValue": number;
    "channelID": number;
    "dataUnit": number;
    "eventCode": number;
    "funcName"?: string;
    "funcType": number;
    "home_id": string;
    "productCode": number;
    "sensorValue": number;
    "sequence": number;
    "timeStamp": number;
    "timeStamp_ms": number;
    "uid": number;
}

export interface PhilioActuatorInterface {
    "uid": string;
    "funcType": string;
    "location": string;
    "userDefineName": string;
    "actuation": Actuation;
    "meter"?: Metering;
    "lastBeatTime": string;
    "lastTamperedTime": string;
    "powerSource": string;
    "currentActuateState": string;
    "controllableBy": string;
}

export interface PhilioSensorInterface {
    "uid": string;
    "location": string;
    "userDefineName": string;
    "description": string;
    "deviceChannel": Array <PhilioSensorChannel>;
    "lastBeatTime": string;         //"yyyy/mm/dd hh:ii:ss"
    "lastTamperedTime": string;     //"yyyy/mm/dd hh:ii:ss"
    "powerSource": string;
    "currentPowerLevel": string     //"66%"

}


export interface PhilioRespDevice {
    Application_Subversion: number;
    Application_Version: number;
    "Manufacture ID": number;
    "Product ID": number;
    "Product type": number;
    "Protocol_Subversion": number;
    "Protocol_Version": number;
    "battery": number;
    "channel": Array<PhilioRespDeviceChannel>;
    "code": string;
    "home_id": string;
    "lasttampertime": string;
    "lasttampertime_utc": number;
    "map": string;
    "uid": number;
}

export type PhilioRespDeviceChannel = {

        "Dim_ON_Value": number;
        "basicvalue": number;
        "chid": number;
        "ctrltype": number;
        "functype": number;
        "generic": number;
        "lastbeattime": string;
        "lastbeattime_utc": number;
        "lowbattnotify": number;
        "mainscene": number;
        "mutichannelassociation": number;
        "name": string;
        "response_time": number;
        "security": number;
        "sensorstate": number;
        "sensorunit": number;
        "sensorvalue": number;
        "specific": number;
        "status_flag": number;
        "switch_all": number;
        "switchcolor": number;
        "tampernotify": number;
        "type": number;
}

type Metering = {
    "kwh"?: string;
    "watt": string;
    "voltage"?: string;
    "current"?: string;
}

type Actuation = {
    "type": string;
    "on": string;       //on value = 255
    "off": string;      //off value = 0
}

type PhilioSensorChannel = {
    "chid": string;
    "funcType": string;
    "name": string;
    "sensorUnit": string;
    "latestSensorValue": string;
    "accumulatedData"?: string;
}