import { PhilioIfConfig } from './philioZwaveInterface'

export interface sceneList extends Array <scene> {
    [index:number]:scene
}

export interface scene {
    "ID": number,
    "comps": Array <string>,
    "invisible": number,
    "title": string
}


export interface JuboSpaceObject {
    self: PhilioIfConfig;
    philio: PhilioIfConfig;
    PhilioChannelList: PhilioChannelListQuery;    
}

export interface ZwaveObject {
    "addnodestatus": number,
    "appverstr": string,
    "capability": number,
    "chiptype": number,
    "chipver": number,
    "ctrlgid": number,
    "homeid": string,
    "isbridge": number,
    "isexistsis": number,
    "isnetworkchanging": number,
    "isnosecure": number,
    "isonothernetwork": number,
    "isrealprimary": number,
    "issecondary": number,
    "issomeonedevicewakeup": number,
    "issuc": number,
    "learnnodestatus": number,
    "libver": number,
    "modulever": number,
    "nodeid": number,
    "nodemap": string,
    "removenodestatus": number,
    "sucid": number
}

export interface SupportInfo {
    [index: number]: string;
}
export interface PhilioGatewayObject {
    "isrfbusy": number,
    "panid": string,
    "protocol": number,
    "uid": number,
    "version": string,
    "zwave": ZwaveObject,
    "branch": string,
    "buildno": number,
    "hostname": string,
    "mac": string,
    "map": string,
    "model": string,
    "revision": number,
    "supportInfo": SupportInfo,
    "uuid": string,
    "deviceVendor": string,
    "deviceIp": string,
    "deviceMac": string,
    "deviceHostname": string | null,
    "isDeviceAlive": boolean,
    "connectOption": PhilioIfConfig   
}

export interface PhilioDeviceListQuery extends Array < PhilioDevice > {
    [label:number]: PhilioDevice;
}

export interface PhilioDevice {
    uid: number;
    productId: string;
    homeId: string;
    group: string;
    userDefineName: string;
    location: string;
    description: string;
    powerLevel: number;
    powerStatus: string;
    tamperStatus: string;
    tamperTimeStamp: number;
    [label:number]: deviceChannel;
}

export interface deviceChannel{
    chid: number;
    functype: string;
    name: string;
}

export interface philioProductInfo {
    productId: string;
    part: string;
    product: string;
}

export interface PhilioChannelListQuery extends Array <PhilioChannel> {
    [label:number]: PhilioChannel;
}
export interface PhilioChannel {
    uid: number;
    chid: number;    
    group: string;
    userDefineName: string;
    location: string;
    homeId: string;
    productId: string;
    functype: string;
    name: string;
    description: string;
    sceneValMax: number;
    sceneValMin: number;
    sceneSetting: sceneSettingQuery;
}

export interface sceneSettingQuery extends Array <sceneSetting> {
    [label:number]: sceneSetting;
}
export interface sceneSetting {
    sceneName: string;
    sceneVal: number;
}

export interface JuboLightSource {
    homeId:string;
    chid:string;
    uid:string;
    val:string;
    location: string;
    group: string;  

}

export interface LightSceneJson extends Array<JuboLightSource> {
    [label: number]: JuboLightSource;
}
export interface JuboLightScene extends Array<JuboLightSource> {
    enteringLight?: {
        [label: number]: JuboLightSource;
    }
    exitingLight?: {
        [label: number]: JuboLightSource;
    }
    restingLight?: {
        [label: number]: JuboLightSource;
    }    
    readingLight?: {
        [label: number]: JuboLightSource;
    }
    sleepingLight?: {
        [label: number]: JuboLightSource;
    }
    morningLight?: {
        [label: number]: JuboLightSource;
    } 
    allOn?: {
        [label: number]: JuboLightSource;
    }  
    allOff?: {
        [label: number]: JuboLightSource;
    }  
    emergencyOn?: {
        [label: number]: JuboLightSource;
    }
}

export interface PhilioRespMessage {
    control: {
        cmd: string;
        respcode: number;
        respmsg: string;
        uid: number;
    }
}




export interface PhilioCmdObject {
    cmd: string;
    uid: number;
    home_id: string;
    optional?: PhilioOptionalCmd;

}
export interface PhilioOptionalCmd {
    datas?: string;
    key?: string;
    val?: number;
    size?: string;
    chid?: number;
    temp?: string;
    unit?: string;
    associateuid?: string;
    associategroup?: string;
}

// Code Reference @ Result L:19 https://github.com/suskind/network-list/blob/master/src/index.js
export interface NetListObjectType {
    ip: string;
    alive: boolean;
    hostname: string;
    mac: string;
    vendor: string;
    hostnameError: string;
    macError: string;
    vendorError: string;
}
export interface PhilioEventLog {
    
    Dim_ON_Value: string;
    Scene_schedule_time: string;
    basicValue: string;
    battery: string;
    channelID: string;
    dataUnit: string;
    eventCode: string;
    funcName: string;
    funcType: string;
    productCode: string;
    sensorValue: string;
    sequence: string;
    timeStamp: string;
    timeStamp_ms: string;
    uid: string;
    mac: string;

}
export interface PhilioSensorLog {
    basicValue: string;
    eventCode: string;
    battery: string;
    dataUnit: string;
    sensorValue: string;
    timeStamp: string;
    funcType: string;
    funcName: string;
    sequence: string;
    uid: string;  
}
export interface PhilioLogTimeSeries {
    tamper: PhilioTamperLog;
    pir: PhilioPirLog;
    door: PhilioDoorLog;
    batteryChange: PhilioBatteryLog;
    batteryLow: PhilioBatteryLog;
    temperature: PhilioTemperatureLog;
    meter: PhilioMeterLog;
    trigger: PhilioTriggerLog;
    lumen: PhilioLumenLog;
    humidity: PhilioHumidityLog;
    inclusion: PhilioInclusionLog;
    exclusion: PhilioExclusionLog;
    status: PhilioStatusLog;
    sequence: string;
}

export interface PhilioTamperLog {
    [index: number]: PhilioSensorLog    
}
export interface PhilioPirLog {
    [index: number]: PhilioSensorLog    
}

export interface PhilioDoorLog {
    [index: number]: PhilioSensorLog    
}
export interface PhilioBatteryLog {
    [index: number]: PhilioSensorLog    
}

export interface PhilioTemperatureLog {
    [index: number]: PhilioSensorLog    
}

export interface PhilioMeterLog {
    [index: number]: PhilioSensorLog    
}

export interface PhilioTriggerLog {
    [index: number]: PhilioSensorLog    
}

export interface PhilioLumenLog {
    [index: number]: PhilioSensorLog    
}

export interface PhilioHumidityLog {
    [index: number]: PhilioSensorLog    
}

export interface PhilioInclusionLog {
    [index: number]: PhilioSensorLog    
}

export interface PhilioExclusionLog {
    [index: number]: PhilioSensorLog    
}

export interface PhilioStatusLog {
    [index: number]: PhilioSensorLog    
}


export interface PhilioRespControl {
    cmd: string;
    respcode: number;
    respmsg: string;
    uid: number
}


export interface smileInnReport {
    "name": string;
    "room": string;
    "type": string;
    "value": smileInnReportObject;
}

export interface smileInnReportObject {

}


