//local test client

const WebSocket = require('ws');

const wsClient = new WebSocket('ws://localhost:8888');


wsClient.onopen = () => {
    wsClient.send('/devicelist')
}

wsClient.onerror = (error) => {
    console.log(`WebSocket error: ${error}`)
}

wsClient.onmessage = (e) => {
    console.log("client ==> \r\n" + e.data)
}