'use strict';
export {};
const winston = require('winston')
const sysProcess = require('process');
const Netlist = require('./netlist/netlist');
const vayyarController = require('./stateMachine/vayyarWalabot/vayyarWalabotGateway')
const philioGateway = require('./stateMachine/philioZwave/philioGateway')
const myHealthApp = require('./stateMachine/myHealthApp/myHealthAppServer')
const mwgMattressController = require('./stateMachine/mwgMattress/mwgGateway')
const JuboDeviceDb = require('./nanoCouchDb/juboSpaceDeviceDb')
const deviceDbHandler = new JuboDeviceDb()

import type { GenericSpaceInfo, GenericTenantInfo } from './interfaces/genericInterface'
import { JuboBoxInfo, 
        simplifyDeviceInfo, 
        simplifyPatientInfo,
        vayyarDeviceDbInterface,
        mwgMattressDbInterface,
        philioDeviceDbInterface,
        iPadDeviceDbInterface } from './interfaces/nanoCouchInterface'

let defaultTenants : Array<GenericTenantInfo> = [
    {
        "patientId": "5ed5f57439baab1d94cdb495",
        "first_name": "咪咪",
        "last_name": "陳",
        "display_name": "Meow",
        "phone": "123-4567",
        "birthday": "1911-11-99",
        "sex": "Alien",
        "email": "123@gmail.com",
        "id_number": "A1234567",
        "roomNum": "100",
        "bedNum": "1",
    }]

const logger = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json(),
        winston.format.prettyPrint()
    ),
    defaultMeta: { service: 'user-service' },
    transport: [
        new winston.transports.File({ filename: 'jubospace-error.log', level:'error' }),
        new winston.transports.File({ filename: 'jubospace-combined.log'}),
        new winston.transports.Console()
    ],
})    

const initJuboBoxInfo = async () => {

    let vayyarDeviceList:Array<vayyarDeviceDbInterface> = await deviceDbHandler.getVayyarDeviceList()
    let mwgMattressList:Array<mwgMattressDbInterface> = await deviceDbHandler.getMwgMattressDeviceList()
    let iPadDeviceList:Array<iPadDeviceDbInterface> = await deviceDbHandler.getIpadDeviceList()
    let philioDeviceList:Array<philioDeviceDbInterface> = await deviceDbHandler.getPhilioDeviceList()
    let juboBoxInfo:JuboBoxInfo = await deviceDbHandler.getJuboBoxInfo()
    
    Promise.all([vayyarDeviceList,
                mwgMattressList,
                iPadDeviceList,
                philioDeviceList,
                juboBoxInfo])
                .catch((error)=>{
                    console.log(`fn: initJuboBoxInfo ${error}`)
                })
                .then((resp)=>{
        let deviceArray:Array<simplifyDeviceInfo> = []
        let patientArray:Array<simplifyPatientInfo> = []           
        let vayyarArray = resp[0]
        let mwgArray = resp[1]
        let ipadArray = resp[2]
        let philioArray = resp[3]
        let juboBoxTemp = resp[4]
        
        if(vayyarArray.length >=1 || vayyarArray != undefined){
            for(const vayyar of vayyarArray){
                if(vayyar != undefined){
                    deviceArray.push({
                        "deviceId": vayyar.deviceId,
                        "vendorId": vayyar.vendorId,
                        "devicePartitionKey": "vayyar",
                        "registeredPatientId": vayyar.patientId
                    })
                }
            }
        }

        if(mwgArray.length >= 1 || mwgArray != undefined){
            for(const mwg of mwgArray){
                if(mwg != undefined){
                    deviceArray.push({
                        "deviceId": mwg.deviceId,
                        "vendorId": mwg.vendorId,
                        "devicePartitionKey": "mwg",
                        "registeredPatientId": mwg.patientId
                    })
                }
            }
        }

        if(ipadArray.length >= 1 || ipadArray != undefined){
            for(const iPad of ipadArray){
                if(iPad != undefined){
                    deviceArray.push({
                        "deviceId": iPad.deviceId,
                        "vendorId": iPad.vendorId,
                        "devicePartitionKey": 'ipad',
                        "registeredPatientId": iPad.patientId
                    })
                }
            }
        }
        if(philioArray.length >= 1 || philioArray != undefined){
            for(const philio of philioArray){
                if(philio != undefined){
                    deviceArray.push({
                        "deviceId": philio.deviceId,
                        "vendorId": philio.vendorId,
                        "devicePartitionKey": 'philio',
                        "registeredPatientId": philio.patientId
                    })
                }
            }
        }

        juboBoxTemp.self.connectedDeviceList = deviceArray
        if(juboBoxTemp.self.createdTime === ''){
            juboBoxTemp.self.createdTime = new Date(Date.now()).toISOString()
        }
        juboBoxTemp.self.updatedTime = new Date(Date.now()).toISOString()
        juboBoxTemp.self.lastBeatTime = new Date(Date.now()).toISOString()
        //juboBoxTemp._rev = (parseInt(juboBoxTemp._rev.split('-')[0])+1)+'-'+juboBoxTemp._rev.split('-')[1]
        //console.log(juboBoxTemp)
        deviceDbHandler.dbInsertUpdateJuboBox(juboBoxTemp).then((resp)=>{
            if(resp.ok === false){
                console.log(`fn:initJuboBoxInfo Update Fail`)
            }
        })
    })
}

const smilinnInit = async () => {
    try{
        let self= await Netlist.findSelf();
        initJuboBoxInfo()
        /*
        logger.log({
            level: 'info',
            message: 'hahahbababa'
        })
        */
        if(self){
            let mySpace : GenericSpaceInfo = {
                address: '新店區民權路132號2樓',
                roomNum: "301",
                numberOfBeds: "1",
                netInfo: self,
                tenants: defaultTenants,
            }
            
            new vayyarController(mySpace,deviceDbHandler)
            new mwgMattressController(mySpace,deviceDbHandler)
            new philioGateway(mySpace)
            new myHealthApp(mySpace,deviceDbHandler)
        }
    } catch (error) {
        console.log(`fn:smileinnInit ${error}`)
    }
}

smilinnInit()

sysProcess.on('uncaughtException', err => {
    console.log(`Uncaught Exception: ${err.message}`)
    sysProcess.exit(1)
})

sysProcess.on("beforeExit", endCode => {
    console.log("Before Exit I execute this after timeout")
    setTimeout(()=>{
        console.log(`Process will exit with code: ${endCode}`)
        process.exit(endCode)
    },100)
})

sysProcess.on('SIGTERM', () => {
    console.log(`Process ${process.pid} received a SIGTERM signal`)
    sysProcess.exit(0)
});

sysProcess.on('SIGINT', signal => {
    console.log(`Process ${sysProcess.pid} has been interrupted`)
    sysProcess.exit(0)
  })

sysProcess.on('exit', function (code) {
    return console.log(`About to exit with code ${code}`);
});
